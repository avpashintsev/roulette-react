import {confirmEmail} from '../store/auth/actions';
import {showPopup} from "../store/popups/actions";
import {setPasswordResetToken} from '../store/auth/actions';
// import {showModal} from '../store/modal/actions';

export function checkReferral() {
	const referral = getURLParameter('ref');

	if (referral) {
		localStorage.setItem('ref', referral);
		setURLWithoutParameter('ref');
	}
}

export function checkResetPassword() {
	return dispatch => {
		const token = getURLParameter('reset-password');

		if (token) {
			dispatch(setPasswordResetToken(token));
			dispatch(showPopup('passwordRecoveryComplete'));
			setURLWithoutParameter('reset-password');
		}
	}
}

export function checkEmailConfirmationCode() {
	return dispatch => {
		const code = getURLParameter('code');
		const email = getURLParameter('email');

		if (code && email) {
			const data = {
				email,
				code,
			};

			dispatch(confirmEmail(data));

			setURLWithoutParameter('email');
			setURLWithoutParameter('code');
		}
	}
}

function getURLParameter(parameter) {
	const url = new URL(window.location.href);
	return url.searchParams.get(parameter);
}

function setURLWithoutParameter(parameter) {
	const newUrl = removeURLParameter(window.location.href, parameter);
	window.history.replaceState({}, document.title, newUrl);
}

function removeURLParameter(url, parameter) {
	let urlparts = url.split('?');
	if (urlparts.length >= 2) {
		const prefix = encodeURIComponent(parameter) + '=';
		const pars = urlparts[1].split(/[&;]/g);

		for (let i = pars.length; i-- > 0;) {
			if (pars[i].lastIndexOf(prefix, 0) !== -1) {
				pars.splice(i, 1);
			}
		}

		return urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : '');
	}
	return url;
}