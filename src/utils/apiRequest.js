import {showErrorPopup} from '../store/popups/actions';

const axiosClient = require('./axiosClient');

export function apiRequest(url) {
	return dispatch => {
		return axiosClient.get(url).then(res => {
				if (res.data.status === 'ok') {
					return res.data.data;
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			err => {
				dispatch(showErrorPopup(err.message));
			}
		);
	}
}