import React from 'react';
import {useSelector} from "react-redux";

export function numberNonExponentialPrecision(number, precision) {
	if (!number) number = 0;
	if (!precision) return number;

	let res = parseFloat(number).toFixed(precision).replace(/\.?0+$/, '');
	res = parseFloat(res);

	return res;
}

export function numberNonExponential(number, isNumber = false) {
	const result = parseFloat(number).toFixed(8).replace(/\.?0+$/, '');

	return isNumber || isFloat(result) ? result : number;
}

export function convertToNumber(number) {
	return parseFloat(number);
}

export function formattingDate(time) {
	const date = new Date(time);
	const year = "0" + date.getYear();
	const month = "0" + (date.getMonth() + 1);
	const day = "0" + date.getDate();
	const hours = "0" + date.getHours();
	const minutes = "0" + date.getMinutes();

	return day.substr(-2) + '/' + month.substr(-2) + '/' + year.substr(-2) + ', ' +
		hours.substr(-2) + ':' + minutes.substr(-2);
}

function isFloat(n) {
	return n === +n && n !== (n | 0);
}

export function secondsToHms(d) {
	d = Number(d);
	const h = Math.floor(d / 3600);
	const m = Math.floor(d % 3600 / 60);
	const s = Math.floor(d % 3600 % 60);

	const hDisplay = h > 0 ? h + (h === 1 ? "h " : "h ") : "";
	const mDisplay = m > 0 ? m + (m === 1 ? "m " : "m ") : "";
	const sDisplay = s > 0 ? s + (s === 1 ? "s" : "s") : "";
	return hDisplay + mDisplay + sDisplay;
}

export function secondsToDhms(seconds) {
	seconds = Number(seconds);
	var d = Math.floor(seconds / (3600 * 24));
	var h = Math.floor(seconds % (3600 * 24) / 3600);
	var m = Math.floor(seconds % 3600 / 60);
	var s = Math.floor(seconds % 60);

	var dDisplay = d > 0 ? d + (d === 1 ? "d " : "d ") : "";
	var hDisplay = h > 0 ? h + (h === 1 ? "h " : "h ") : "";
	var mDisplay = m > 0 ? m + (m === 1 ? "m " : "m ") : "";
	var sDisplay = s > 0 ? s + (s === 1 ? "" : "") : "";
	return dDisplay + hDisplay + mDisplay + sDisplay;
}

export function formatNumber(x) {
	x = parseFloat(Number(x).toFixed(12));

	if (Math.abs(x) < 1.0) {
		var e = parseInt(x.toString().split('e-')[1]);
		if (e) {
			x *= Math.pow(10, e - 1);
			x = '0.' + (new Array(e)).join('0') + x.toString().substring(2);
		}
	} else {
		var e = parseInt(x.toString().split('+')[1]);
		if (e > 20) {
			e -= 20;
			x /= Math.pow(10, e);
			x += (new Array(e + 1)).join('0');
		}
	}
	return x;
}

export function priceConvertAndRound(num, price, currency, precision = 2) {
	if (num === undefined) return '';

	num = formatNumber(num);

	return currency === 'BTC' ? num : (num * price).toFixed(precision);
}

export function priceBackConvertAndRound(num, price = 1, currency, precision = 4) {
	if (num === undefined) return '';

	num = formatNumber(num);

	return currency === 'BTC' ? (num / price).toFixed(precision) : num.toFixed(0);
}
