import Validator from 'validator';
import isEmpty from 'lodash/isEmpty';
import WAValidator from 'wallet-address-validator';

export default function validateInput(data) {
	let errors = {};
	if (data.hasOwnProperty('agree')) {
		if (!data.agree) {
			errors.agree = 'youMustAgree';
		}
	}
	if (data.hasOwnProperty('email')) {
		if (!Validator.isEmail(data.email)) {
			errors.email = 'emailIsInvalid';
		}
		if (Validator.isEmpty(data.email)) {
			errors.email = 'thisFieldIsRequired';
		}
	}
	if (data.hasOwnProperty('password')) {
		if (!Validator.isLength(data.password, {min: 8})) {
			errors.password = 'passwordCheck';
		}
		if (Validator.isEmpty(data.password)) {
			errors.password = 'thisFieldIsRequired';
		}
	}
	if (data.hasOwnProperty('confirmPassword')) {
		if (!Validator.equals(data.password, data.confirmPassword)) {
			errors.confirmPassword = 'passwordsMustMatch';
		}
		if (Validator.isEmpty(data.confirmPassword)) {
			errors.confirmPassword = 'thisFieldIsRequired';
		}
	}
	// if (data.hasOwnProperty('withdrawalAmount')) {
	// 	const min = minWithdrawal[data.walletSymbol];
	//
	// 	if (data.withdrawalAmount < min) {
	// 		errors.withdrawalAmount = `Min ${min} ${data.walletSymbol}`;
	// 	}
	// }
	if (data.hasOwnProperty('wallet')) {
		if (!WAValidator.validate(data.wallet, 'BTC')) {
			errors.wallet = 'notValid';
		}
		if (Validator.isEmpty(data.wallet)) {
			errors.wallet = 'thisFieldIsRequired';
		}
	}
	// if (data.hasOwnProperty('amount')) {
	// 	const min = minBet[data.currency];
	//
	// 	if (data.amount < min) {
	// 		errors.amount = `Min ${min} ${data.currency}`;
	// 	}
	// }

	return {
		errors,
		isValid: isEmpty(errors)
	}
}