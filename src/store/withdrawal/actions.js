import {showErrorPopup, showSuccessPopup} from '../popups/actions';
import {getUserData} from '../auth/actions';
const axiosClient = require('../../utils/axiosClient');

export function withdrawal(data) {
	return (dispatch, getState) => {
		const t = getState().i18n.translations[getState().i18n.locale];

		return axiosClient.post('/user/withdraw', data).then(res => {
				if (res.data.status === 'ok') {
					dispatch(getUserData());
					dispatch(showSuccessPopup(t.withdrawalConfirmed + ' ' + data.address));
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			(err) => {
				dispatch(showErrorPopup(err.message));
			}
		);
	}
}