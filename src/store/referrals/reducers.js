import {SET_REFERRALS} from './actions';

const initialState = {
	users: [],
	count: '',
};

export const referralReducer = (state = initialState, action) => {
	switch (action.type) {
		case SET_REFERRALS:
			return {
				users: action.payload.users,
				count: action.payload.count,
			};
		case 'RESET':
			return initialState;
		default:
			return state;
	}
};