import {showErrorPopup} from '../popups/actions';
const axiosClient = require('../../utils/axiosClient');

export const SET_REFERRALS = 'SET_REFERRALS';

export function getReferrals() {
	return dispatch => {
		return axiosClient.get('referral/list').then(res => {
				if (res.data.status === 'ok') {
					dispatch(setReferrals(res.data.data));
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			(err) => {
				dispatch(showErrorPopup(err.message));
			}
		);
	}
}

export function setReferrals(data) {
	return {
		type: SET_REFERRALS,
		payload: data
	}
}