import {showErrorPopup} from '../popups/actions';
import {getUserData} from '../auth/actions';
const axiosClient = require('../../utils/axiosClient');

export const UPDATE_LAST_GAME_RESULTS = 'UPDATE_LAST_GAME_RESULTS';

export function getLastEarn() {
	return dispatch => {
		axiosClient.get('/earn/last').then(
			res => {
				if (res.data.status === 'ok') {
					if (res.data.data) {
						const lastGameTime = Math.floor(Date.parse(res.data.data.updated_at) / 1000);
						const nextGameTime = lastGameTime + 86400;

						const lastGame = {
							lastGameTime,
							nextGameTime,
							winNumber: res.data.data.win_number,
							winSum: res.data.data.win_sum,
						};

						dispatch(updateLastGameResults(lastGame));
					}
				} else {
					showErrorPopup(res.data.message);
				}
			},
			err => {
				showErrorPopup(err.message);
			},
		)
	}
}

export function runNewEarn() {
	return (dispatch, getState) => {
		const {isAuthenticated} = getState().authReducer;
		const {nextGameTime} = getState().earnReducer.data;
		const t = getState().i18n.translations[getState().i18n.locale];

		if (!isAuthenticated) {
			dispatch(showErrorPopup(t.pleaseLogIn));
		} else if (Math.floor(Date.now() / 1000) < nextGameTime) {
			dispatch(showErrorPopup(t.alreadyRun));
		} else {
			axiosClient.post('/earn/run').then(
				res => {
					if (res.data.status === 'ok') {
						if (res.data.data) {
							const lastGameTime = Math.floor(Date.parse(res.data.data.created_at) / 1000);
							const nextGameTime = lastGameTime + 86400;

							const lastGame = {
								lastGameTime,
								nextGameTime,
								winNumber: res.data.data.win_number,
								winSum: res.data.data.win_sum,
							};

							dispatch(updateLastGameResults(lastGame));
							dispatch(getUserData());
						}
					} else {
						dispatch(showErrorPopup(res.data.message));
					}
				},
				err => {
					dispatch(showErrorPopup(err.message));
				},
			)
		}
	}
}

export function updateLastGameResults(lastGame) {
	return {
		type: UPDATE_LAST_GAME_RESULTS,
		payload: lastGame
	}
}