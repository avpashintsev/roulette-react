import {UPDATE_LAST_GAME_RESULTS} from './actions';

const initialState = {
	data: {}
};

export const earnReducer = (state = initialState, action) => {
	switch (action.type) {
		case UPDATE_LAST_GAME_RESULTS:
			return {
				data: action.payload
			};
		case 'RESET':
			return initialState;
		default:
			return state;
	}
};