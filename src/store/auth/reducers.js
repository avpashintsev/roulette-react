import {
	SET_CURRENT_USER,
	SET_NOT_CONFIRMED_EMAIL,
	CONFIRM_EMAIL,
	GET_USER_DATA,
	USER_DEPOSIT_WALLETS,
	SET_ADDRESS,
	SET_PASSWORD_RESET_TOKEN
} from './actionTypes';
import isEmpty from 'lodash/isEmpty';

const initialState = {
	// notConfirmedEmail: '',
	isAuthenticated: false,
	user: {},
	info: {},
	wallets: {},
	address: [],
	passwordResetToken: '',
};

export const authReducer = (state = initialState, action) => {
	switch (action.type) {
		case SET_CURRENT_USER:
			return {
				...state,
				isAuthenticated: !isEmpty(action.payload),
				user: action.payload
			};
		case SET_NOT_CONFIRMED_EMAIL:
			return {
				...state,
				notConfirmedEmail: action.payload
			};
		case GET_USER_DATA:
			
			alert(JSON.stringify(action.payload))
			return {
				...state,
				info: action.payload
			};
		// case CONFIRM_EMAIL:
		// 	return {
		// 		...state,
		// 		notConfirmedEmail: ''
		// 	};
		case USER_DEPOSIT_WALLETS:
			return {
				...state,
				wallets: action.payload,
			};
		case SET_ADDRESS:
			return {
				...state,
				address: action.payload,
			};
		case SET_PASSWORD_RESET_TOKEN:
			return {
				...state,
				passwordResetToken: action.payload,
			};
		case 'RESET':
			return initialState;
		default:
			return state;
	}
};