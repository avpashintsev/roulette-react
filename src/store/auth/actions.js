import jwtDecode from 'jwt-decode';
import {
	SET_CURRENT_USER, SET_NOT_CONFIRMED_EMAIL, CONFIRM_EMAIL,
	GET_USER_DATA, USER_DEPOSIT_WALLETS, SET_PASSWORD_RESET_TOKEN, SET_ADDRESS
} from './actionTypes';
import {showErrorPopup, showSuccessPopup, hidePopup, showPopup} from '../popups/actions';

const {apiRequest} = require('../../utils/apiRequest');
const axiosClient = require('../../utils/axiosClient');

export function setCurrentUser(user) {
	return {type: SET_CURRENT_USER, payload: user};
}

// export function setNotConfirmedEmail(email) {
// 	return {type: SET_NOT_CONFIRMED_EMAIL, payload: email};
// }

export function confirmEmailSuccess() {
	return {type: CONFIRM_EMAIL};
}

export function setPasswordResetToken(token) {
	return {type: SET_PASSWORD_RESET_TOKEN, payload: token}
}

export function getUserDataRequestSuccess(data) {
	return {type: GET_USER_DATA, payload: data}
}

export function getUserDepositWalletsSuccess(wallets) {
	return {type: USER_DEPOSIT_WALLETS, payload: wallets}
}

export function getAddressSuccess(data) {
	return {type: SET_ADDRESS, payload: data}
}

export function checkAuth(store) {
	if (localStorage.jwtToken) {
		const tokenExpire = jwtDecode(localStorage.jwtToken).exp;
		const currentTimestamp = Math.floor(Date.now() / 1000);

		if (currentTimestamp > tokenExpire) {
			localStorage.removeItem('jwtToken');
		} else {
			setAuthorizationToken(localStorage.jwtToken);
			store.dispatch(setCurrentUser(jwtDecode(localStorage.jwtToken)));
			store.dispatch(getUserData());
		}
	}
}

export function setAuthorizationToken(token) {
	if (token) {
		localStorage.setItem('jwtToken', token);
		axiosClient.defaults.headers.common['Auth'] = token;
	} else {
		localStorage.removeItem('jwtToken');
		delete axiosClient.defaults.headers.common['Auth'];
	}
}

export function getUserData() {
	return dispatch => {
		return axiosClient.get('/user/info').then(res => {
				if (res.data.status === 'ok') {
					dispatch(getUserDataRequestSuccess(res.data.data));
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			err => {
				dispatch(showErrorPopup(err.message));
			}
		);
	}
}

export function signUp(userData) {
	return dispatch => {
		return axiosClient.post('/user/create', userData).then(res => {
				dispatch(hidePopup('signUp'));
				if (res.data.status === 'ok') {
					dispatch(showPopup('confirmEmail'));
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			err => {
				dispatch(hidePopup('signUp'));
				dispatch(showErrorPopup(err.message));
			}
		);
	}
}

export function logIn(userData) {
	return dispatch => {
		return axiosClient.post('/user/login', userData).then(res => {
				if (res.data.status === 'ok') {
					const token = res.data.data.token;

					setAuthorizationToken(token);

					dispatch(setCurrentUser(jwtDecode(token)));

					dispatch(getUserData()).then(() => {
						dispatch(hidePopup('logIn'));
					});
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			err => {
				dispatch(hidePopup('logIn'));
				dispatch(showErrorPopup(err.message));
			}
		);
	}
}

export function logOut() {
	console.log('logout');

	return dispatch => {
		setAuthorizationToken(false);
		dispatch({type: 'RESET'});
	}
}

export function updateUserData(data) {
	return (dispatch, getState) => {
		const t = getState().i18n.translations[getState().i18n.locale];

		return axiosClient.post('/user/profile-update', data).then(res => {
				if (res.data.status === 'ok') {
					dispatch(showSuccessPopup(t.successfullyUpdated));
					dispatch(getUserData());
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			(err) => {
				dispatch(showErrorPopup(err.message));
			}
		);
	}
}

export function updateAvatar(avatar_number) {
	return (dispatch) => {
		return axiosClient.post('/user/profile-update', {avatar_number}).then(res => {
				if (res.data.status === 'ok') {
					dispatch(getUserData());
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			(err) => {
				dispatch(showErrorPopup(err.message));
			}
		);
	}
}

export function updatePasswords(data) {
	return (dispatch, getState) => {
		const t = getState().i18n.translations[getState().i18n.locale];

		return axiosClient.post('/user/update-password', data).then(res => {
				if (res.data.status === 'ok') {
					dispatch(showSuccessPopup(t.successfullyUpdated));
					dispatch(getUserData());
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			(err) => {
				dispatch(showErrorPopup(err.message));
			}
		);
	}
}

export function getUserDepositWallets() {
	return dispatch => {
		axiosClient.post('/user/get_deposit_address').then(
			res => {
				if (res.data.status === 'ok') {
					dispatch(getUserDepositWalletsSuccess(res.data.data.addresses));
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			err => {
				dispatch(showErrorPopup(err.message));
			},
		);
	}
}

export function confirmEmail(data) {
	return (dispatch, getState) => {
		const t = getState().i18n.translations[getState().i18n.locale];

		axiosClient.post('/user/confirm', data).then(
			res => {
				if (res.data.status === 'ok') {
					dispatch(hidePopup('confirmEmail'));
					dispatch(showSuccessPopup(t.congratulations));
					dispatch(showPopup('logIn'));
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			err => {
				dispatch(showErrorPopup(err.message));
			},
		)
	}
}

export function getAddress() {
	return dispatch => {
		return axiosClient.get('/user/address-list').then(res => {
				if (res.data.status === 'ok') {
					dispatch(getAddressSuccess(res.data.data));
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			(err) => {
				dispatch(showErrorPopup(err.message));
			}
		);
	}
}

export function addAddress(data) {
	return (dispatch, getState) => {
		const t = getState().i18n.translations[getState().i18n.locale];

		return axiosClient.post('/user/add-address', data).then(res => {
				if (res.data.status === 'ok') {
					dispatch(getAddress());
					dispatch(hidePopup('addAddress'));
					dispatch(showSuccessPopup( t.congratulationsBTC));
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			(err) => {
				dispatch(showErrorPopup(err.message));
			}
		);
	}
}

export function removeAddress(id) {
	return dispatch => {
		return axiosClient.post('/user/delete-address', {id}).then(res => {
				if (res.data.status === 'ok') {
					dispatch(getAddress());
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			(err) => {
				dispatch(showErrorPopup(err.message));
			}
		);
	}
}

export function recoverRequest(email) {
	return dispatch => {
		return axiosClient.post('/user/change-password-request', {email}).then(res => {
				if (res.data.status === 'ok') {
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			(err) => {
				dispatch(showErrorPopup(err.message));
			}
		);
	}
}

export function changePasswordComplete(data) {
	return (dispatch, getState) => {
		const t = getState().i18n.translations[getState().i18n.locale];

		axiosClient.post('/user/change-password-complete', data).then(
			res => {
				if (res.data.status === 'ok') {
					dispatch(showSuccessPopup(t.successfulPasswordChange));
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			err => {
				dispatch(showErrorPopup(err.message));
			},
		);
	}
}