export interface Balance {
	BTC: number;
	ETH: number;
}

export interface Statistics {
	gamesTotal: number;
	totalPaid: number;
	winTotal: number;
	topLuck: number;
}
export interface User {
	email: string;
	phone_number: string;
	nickname: string;
	avatar_number: number;
	isEmailConfirm: boolean;
	createdAt: Date;
	referralKey: string;
	balance: Balance;
	isEnabledTwoFactor: boolean;
	statistics: Statistics;
}

export interface Address{
    id: number
    address: string
    currency: string
  }