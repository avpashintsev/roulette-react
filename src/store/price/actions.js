import {UPDATE_PRICE, SET_CURRENCY_TYPE} from './actionTypes';
import {showErrorPopup, showSuccessPopup, hidePopup, showPopup} from '../popups/actions';
import isEmpty from "lodash/isEmpty";
import {numberNonExponential} from "../../utils/format";

const axiosClient = require('../../utils/axiosClient');

export function getPriceSuccess(price) {
	return {type: UPDATE_PRICE, payload: price}
}

export function setCurrencyType(curr) {
	return {type: SET_CURRENCY_TYPE, payload: curr}
}

export function getPrice() {
	return dispatch => {
		axiosClient.get('/price?currency=BTC').then(
			res => {
				if (res.data.status === 'ok') {
					dispatch(getPriceSuccess(res.data.data.price));
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			err => {
				dispatch(showErrorPopup(err.message));
			},
		)
	}
}

export function switchCurrency() {
	return dispatch => {
		const currCurrency = localStorage.curr || 'BTC';
		const newCurr = currCurrency === 'BTC' ? 'USD' : 'BTC';
		localStorage.setItem('curr', newCurr);
		dispatch(setCurrencyType(newCurr));
	}
}