import {UPDATE_PRICE, SET_CURRENCY_TYPE} from './actionTypes';

const initialState = {
	price: '',
	currency: 'BTC',
	convertPrice: 1,
};

export const priceReducer = (state = initialState, action) => {
	switch (action.type) {
		case UPDATE_PRICE:
			const price = parseFloat(action.payload.toFixed(2));

			return {
				...state,
				price,
				convertPrice: state.currency === 'BTC' ? 1 : price,
			};
		case SET_CURRENCY_TYPE:
			return {
				...state,
				currency: action.payload,
				convertPrice: action.payload === 'BTC' ? 1 : state.convertPrice,
			};
		default:
			return state;
	}
};