import {showErrorPopup} from '../popups/actions';
import {SET_RATING, SET_STATS} from './actionTypes';

const axiosClient = require('../../utils/axiosClient');

export function getRating() {
	return dispatch => {
		return axiosClient.get('/rating').then(res => {
				if (res.data.status === 'ok') {
					dispatch(setRating(res.data.data));
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			(err) => {
				dispatch(showErrorPopup(err.message));
			}
		);
	}
}

export function getStats() {
	return dispatch => {
		return axiosClient.get('/statistic').then(res => {
				if (res.data.status === 'ok') {
					dispatch(setStats(res.data.data));
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			(err) => {
				dispatch(showErrorPopup(err.message));
			}
		);
	}
}

export function setRating(data) {
	return {
		type: SET_RATING,
		payload: data
	}
}

export function setStats(data) {
	return {
		type: SET_STATS,
		payload: data
	}
}