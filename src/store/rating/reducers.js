import {SET_RATING, SET_STATS} from './actionTypes';

const initialState = {
	rating: {},
	stats: {},
};

export const ratingReducer = (state = initialState, action) => {
	switch (action.type) {
		case SET_RATING:
			return {
				...state,
				rating: action.payload,
			};
		case SET_STATS:
			return {
				...state,
				stats: action.payload,
			};
		default:
			return state;
	}
};