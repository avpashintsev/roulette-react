import {SHOW_SUCCESS_POPUP, SHOW_ERROR_POPUP, SHOW_POPUP, HIDE_POPUP} from './actions';

const initialState = {
	betInfo: {visible: false},
	confirmEmail: {visible: false},
	forgotPass: {visible: false},
	game: {visible: false},
	logIn: {visible: false},
	signUp: {visible: false},
	addAddress: {visible: false},
	changeAvatar: {visible: false},
	passwordRecoveryComplete: {visible: false},
	showWinner: {visible: false},
	errorPopup: {
		visible: false,
		text: 'Error.',
	},
	successPopup: {
		visible: false,
		text: 'Success.',
	},
};

export const popupReducer = (state = initialState, action) => {
	switch (action.type) {
		case SHOW_SUCCESS_POPUP:
			return {
				...state,
				successPopup: {
					visible: true,
					text: action.payload,
				}
			};
		case SHOW_ERROR_POPUP:
			return {
				...state,
				errorPopup: {
					visible: true,
					text: action.payload,
				}
			};
		case SHOW_POPUP:
			return {
				...state,
				[action.payload]: {visible: true}
			};
		case HIDE_POPUP:
			return {
				...state,
				[action.payload]: {visible: false}
			};
		default:
			return state;
	}
};