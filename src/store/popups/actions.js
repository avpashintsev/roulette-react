export const SHOW_ERROR_POPUP = 'SHOW_ERROR_POPUP';
export const SHOW_SUCCESS_POPUP = 'SHOW_SUCCESS_POPUP';
export const SHOW_POPUP = 'SHOW_POPUP';
export const HIDE_POPUP = 'HIDE_POPUP';

export const showPopup = (modal) => ({
	type: SHOW_POPUP,
	payload: modal
});

export const hidePopup = (modal) => ({
	type: HIDE_POPUP,
	payload: modal
});

export const showSuccessPopup = (text) => ({
	type: SHOW_SUCCESS_POPUP,
	payload: text
});


export const showErrorPopup = (text) => ({
	type: SHOW_ERROR_POPUP,
	payload: text
});
