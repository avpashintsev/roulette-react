import {combineReducers} from 'redux';
import {popupReducer} from './popups/reducers'
import {authReducer} from './auth/reducers'
import {earnReducer} from './earn/reducers'
import {referralReducer} from './referrals/reducers'
import {etherollReducer} from './etheroll/reducers'
import {ratingReducer} from './rating/reducers'
import {rouletteReducer} from './roulette/reducers'
import {priceReducer} from './price/reducers'
import {i18nReducer} from 'react-redux-i18n';

export default combineReducers({
	popupReducer,
	authReducer,
	earnReducer,
	referralReducer,
	etherollReducer,
	ratingReducer,
	rouletteReducer,
	priceReducer,
	i18n: i18nReducer,
});