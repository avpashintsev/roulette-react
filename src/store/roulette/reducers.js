import {ROULETTE_STATUS, ROULETTE_HISTORY, SET_WINNER} from './actionTypes';

const initialState = {
	status: {},
	history: {},
	members: [],
	winner: {},
};

export const rouletteReducer = (state = initialState, action) => {
	switch (action.type) {
		case ROULETTE_STATUS:
			const members = [];

			if (action.payload.members && action.payload.members.length) {
				action.payload.members.forEach(m => {
						if (!members.includes(m.nickname)) {
							members.push(m.nickname);
					}
				});
			}

			return {
				...state,
				status: action.payload,
				members,
			};
		case ROULETTE_HISTORY:
			return {
				...state,
				history: action.payload
			};
		case SET_WINNER:
			return {
				...state,
				winner: action.payload
			};
		case 'RESET':
			return initialState;
		default:
			return state;
	}
};