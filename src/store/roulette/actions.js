import {showErrorPopup, showPopup} from '../popups/actions';
import {ROULETTE_STATUS, ROULETTE_HISTORY, SET_WINNER} from './actionTypes';
import {getUserData} from "../auth/actions";

const axiosClient = require('../../utils/axiosClient');

export function getStatus(gameMode, gameType) {
	return dispatch => {
		return axiosClient.get(`/roulette/status?type=${gameMode}&duration=${gameType}`).then(
			res => {
				if (res.data.status === 'ok') {
					dispatch(getStatusSuccess(res.data.data));
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			err => {
				dispatch(showErrorPopup(err.message));
			},
		)
	}
}

export function getTicket(data) {
	return dispatch => {
		return axiosClient.post('/roulette/getTicket', data).then(
			res => {
				if (res.data.status === "ok") {
					dispatch(getUserData());
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			err => {
				dispatch(showErrorPopup(err.message));
			},
		)
	}
}

export function getRouletteInfo(id) {
	return dispatch => {
		return axiosClient.get('/roulette/info?id=' + id).then(
			res => {
				if (res.data.status === "ok") {
					dispatch(setWinner(res.data.data));
					dispatch(showPopup('showWinner'));
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			err => {
				dispatch(showErrorPopup(err.message));
			},
		)
	}
}

export function getRouletteHistory(page = 1, onlyUser = false) {
	const onlyUserStr = onlyUser ? '&onlyUser=' + onlyUser : '';

	return dispatch => {
		return axiosClient.get('/roulette/history?page=' + page + onlyUserStr).then(
			res => {
				if (res.data.status === 'ok') {
					dispatch(getHistorySuccess(res.data.data));
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			err => {
				dispatch(showErrorPopup(err.message));
			},
		)
	}
}

export function getStatusSuccess(data) {
	return {
		type: ROULETTE_STATUS,
		payload: data
	}
}

export function getHistorySuccess(data) {
	return {
		type: ROULETTE_HISTORY,
		payload: data
	}
}

export function setWinner(data) {
	return {
		type: SET_WINNER,
		payload: data
	}
}