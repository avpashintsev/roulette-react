import {showErrorPopup} from '../popups/actions';
import {getUserData} from '../auth/actions';
const axiosClient = require('../../utils/axiosClient');

export const ETHEROLL_INIT_GAME = 'ETHEROLL_INIT_GAME';
export const ETHEROLL_FINISH_GAME = 'ETHEROLL_FINISH_GAME';
export const ETHEROLL_HISTORY = 'ETHEROLL_HISTORY';

export function initGame() {
	return dispatch => {
		return axiosClient.post('/etherroll/create').then(
			res => {
				if (res.data.status === 'ok') {
					const data = {
						proof: res.data.data.proof,
						gameId: res.data.data.id,
					};

					dispatch(initGameSuccess(data));
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			err => {
				dispatch(showErrorPopup(err.message));
			},
		)
	}
}

export function runEtheroll(data) {
	return dispatch => {
		return axiosClient.post('/etherroll/process', data).then(
			res => {
				if (res.data.status === 'ok') {
					const data = {
						isWin: res.data.data.is_win,
						winNumber: res.data.data.win_number,
						winSecret: res.data.data.win_secret,
					};

					dispatch(runGameSuccess(data));
					dispatch(getUserData());

				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			err => {
				dispatch(showErrorPopup(err.message));
			},
		)
	}
}

export function getEtherollHistory(page = 1, onlyUser = false) {
	const onlyUserStr = onlyUser ? '&onlyUser=' + onlyUser : '';

	return dispatch => {
		return axiosClient.get('/etherroll/history?page=' + page + onlyUserStr).then(
			res => {
				if (res.data.status === 'ok') {
					dispatch(getEtherollHistorySuccess(res.data.data));
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			err => {
				dispatch(showErrorPopup(err.message));
			},
		)
	}
}

export function getEtherollHistorySuccess(data) {
	return {
		type: ETHEROLL_HISTORY,
		payload: data
	}
}

export function initGameSuccess(data) {
	return {
		type: ETHEROLL_INIT_GAME,
		payload: data
	}
}

export function runGameSuccess(data) {
	return {
		type: ETHEROLL_FINISH_GAME,
		payload: data
	}
}