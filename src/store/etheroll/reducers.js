import {ETHEROLL_INIT_GAME, ETHEROLL_FINISH_GAME, ETHEROLL_HISTORY} from './actions';

const initialState = {
	init: {},
	finish: {},
	history: {},
};

export const etherollReducer = (state = initialState, action) => {
	switch (action.type) {
		case ETHEROLL_INIT_GAME:
			return {
				...state,
				init: action.payload,
				finish: {}
			};
		case ETHEROLL_FINISH_GAME:
			return {
				...state,
				finish: action.payload
			};
		case ETHEROLL_HISTORY:
			return {
				...state,
				history: action.payload
			};
		case 'RESET':
			return initialState;
		default:
			return state;
	}
};