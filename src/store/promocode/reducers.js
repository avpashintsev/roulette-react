import {USE_PROMOCODE} from './actions';

const initialState = {
	balance: {}
};

export const promocodeReducer = (state = initialState, action) => {
	switch (action.type) {
		case USE_PROMOCODE:
			return {
				data: action.payload
			};
		default:
			return state;
	}
};