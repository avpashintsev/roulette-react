export const USE_PROMOCODE = 'USE_PROMOCODE';

export const showPopup = (upValue) => ({
	type: USE_PROMOCODE,
	payload: upValue
});