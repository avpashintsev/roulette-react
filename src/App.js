import React, {useEffect} from 'react';
import {Route} from 'react-router-dom';
import Header from './components/Header';
import Stats from './components/Stats';
import Roulette from './components/Roulette';
import Moreless from './components/Moreless';
import Freebtc from './components/Freebtc';
import Rating from './components/Rating';
import Profile from './components/Profile';
import Deposit from './components/Deposit';
import Withdrawal from './components/Withdrawal';
import Footer from './components/Footer';
import BetInfo from './components/popups/BetInfo';
import ConfirmEmail from './components/popups/ConfirmEmail';
import ForgotPass from './components/popups/ForgotPass';
import Game from './components/popups/Game';
import LogIn from './components/popups/LogIn';
import SignUp from './components/popups/SignUp';
import Error from './components/popups/Error';
import Success from './components/popups/Success';
import AddAddress from './components/popups/AddAddress';
import ChangeAvatar from './components/popups/ChangeAvatar';
import ForgotPassComplete from './components/popups/ForgotPassComplete';
import {useDispatch, useSelector} from "react-redux";
import {getUserData} from "./store/auth/actions";
import ShowWinner from "./components/popups/ShowWinner";
import LicenseAgreement from "./components/LicenseAgreement";
import PaymentProof from './components/PaymentProof';
import FAQ from './components/FAQ';

function App() {
	const betInfo = useSelector(state => state.popupReducer.betInfo.visible);
	const confirmEmail = useSelector(state => state.popupReducer.confirmEmail.visible);
	const forgotPass = useSelector(state => state.popupReducer.forgotPass.visible);
	const game = useSelector(state => state.popupReducer.game.visible);
	const logIn = useSelector(state => state.popupReducer.logIn.visible);
	const signUp = useSelector(state => state.popupReducer.signUp.visible);
	const errorPopup = useSelector(state => state.popupReducer.errorPopup.visible);
	const successPopup = useSelector(state => state.popupReducer.successPopup.visible);
	const addAddress = useSelector(state => state.popupReducer.addAddress.visible);
	const changeAvatar = useSelector(state => state.popupReducer.changeAvatar.visible);
	const passwordRecoveryComplete = useSelector(state => state.popupReducer.passwordRecoveryComplete.visible);
	const showWinner = useSelector(state => state.popupReducer.showWinner.visible);
	const isAuthenticated = useSelector(state => state.authReducer.isAuthenticated);

	const dispatch = useDispatch();
/*долбит сервер чтобы проверить авторизацию
	useEffect(() => {
		const userDataInterval = setInterval(() => {
			if (!localStorage.jwtToken) {
				clearInterval(userDataInterval);
			} else {
				dispatch(getUserData());
			}
		}, 2000);
	}, [isAuthenticated]);
*/
	return (
		<div className="App">
			{betInfo && <BetInfo/>}
			{confirmEmail && <ConfirmEmail/>}
			{forgotPass && <ForgotPass/>}
			{game && <Game/>}
			{logIn && <LogIn/>}
			{signUp && <SignUp/>}
			{addAddress && <AddAddress/>}
			{errorPopup && <Error/>}
			{successPopup && <Success/>}
			{changeAvatar && <ChangeAvatar/>}
			{passwordRecoveryComplete && <ForgotPassComplete/>}
			{showWinner && <ShowWinner/>}
			<div className="content">
				<Header/>
				<Stats/>
				<Route exact path='/' component={Roulette}/>
				<Route path='/moreless' component={Moreless}/>
				<Route path='/freebtc' component={Freebtc}/>
				<Route path='/rating' component={Rating}/>
				<Route path='/profile' component={Profile}/>
				<Route path='/deposit' component={Deposit}/>
				<Route path='/withdrawal' component={Withdrawal}/>
				<Route path='/licenseagreement' component={LicenseAgreement}/>	
				<Route path='/paymentproof' component={PaymentProof}/>	
				<Route path='/faq' component={FAQ}/>			
				<Footer/>
			</div>
		</div>
	);
}

export default App;