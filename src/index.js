import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import {createStore, applyMiddleware} from "redux";
import thunk from "redux-thunk";
import {Provider} from "react-redux";
import {composeWithDevTools} from "redux-devtools-extension";
import rootReducer from "./store/rootReducer";
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {checkAuth} from './store/auth/actions';
import {checkReferral, checkEmailConfirmationCode, checkResetPassword} from './utils/checkGetParam';
import {loadTranslations, setLocale, syncTranslationWithStore} from 'react-redux-i18n';
import translations from './locale';
import {setCurrencyType} from "./store/price/actions";

const store = createStore(
	rootReducer,
	composeWithDevTools(applyMiddleware(thunk))
);

syncTranslationWithStore(store);

checkReferral();
checkAuth(store);
store.dispatch(loadTranslations(translations));
store.dispatch(setLocale(localStorage.lang || 'en'));
store.dispatch(checkEmailConfirmationCode());
store.dispatch(checkResetPassword());
store.dispatch(setCurrencyType(localStorage.curr || 'BTC'));

ReactDOM.render((
	<BrowserRouter>
		<Provider store={store}>
			<App/>
		</Provider>
	</BrowserRouter>
), document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
