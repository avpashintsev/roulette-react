import React, {useEffect, useState} from 'react';
import {Link,NavLink} from 'react-router-dom';
import {useDispatch, useSelector} from "react-redux";
import {showPopup} from "../store/popups/actions";
import isEmpty from "lodash/isEmpty";
import {numberNonExponential} from "../utils/format";
import {getPrice, switchCurrency} from "../store/price/actions";
import {setLocale} from 'react-redux-i18n';
import EnImg from '../img/en';
import RuImg from '../img/ru';
import LogoWheel from './logoWheel/logoWheel';
import {logOut} from "../store/auth/actions";

function Header() {
	const [mobNavVisibility, setMobNavVisibility] = useState(false);
	const [playNavVisibility, setPlayNavVisibility] = useState(false);
	const [open, setOpen] = useState(false);
	const [langNow, setLangNow] = useState(localStorage.lang || 'en');
	const [balanceValue, setBalanceValue] = useState('');

	const dispatch = useDispatch();

	const priceBTC = useSelector(state => state.priceReducer.price);
	const isAuthenticated = useSelector(state => state.authReducer.isAuthenticated);
	const info = useSelector(state => state.authReducer.info);
	const currency = useSelector(state => state.priceReducer.currency);
	const t = useSelector(state => state.i18n.translations[state.i18n.locale]);

	useEffect(() => {
		dispatch(getPrice());
	}, []);

	useEffect(() => {
		if (!isEmpty(info)) {
			const balanceBTC = numberNonExponential(info.balance.BTC, true);
			const balanceUSD = (priceBTC * balanceBTC).toFixed(2);

			if (currency === 'BTC') {
				setBalanceValue(balanceBTC)
			} else {
				setBalanceValue(balanceUSD)
			}
		}
	}, [currency, info, priceBTC]);

	const setLang = lang => {
		localStorage.setItem('lang', lang);
		setOpen(false);
		dispatch(setLocale(lang));
		setLangNow(lang);
	};

	return (
		<div className='header'>
			<div className="left">
				<Link to="/">
					<LogoWheel></LogoWheel>
				</Link>
				<div className="header__nav">					
					<div className="firstLane">
						<NavLink to="/" className="navLink rouletteMenuItem" activeClassName="selectedTab" onClick={() => setPlayNavVisibility(false)}>{t.roulette}</NavLink>
					</div>
					<div className="firstLane">
						<NavLink to="/moreless" className="navLink diceMenuItem" activeClassName="selectedTab" onClick={() => setPlayNavVisibility(false)}>{t.moreLess}</NavLink>
					</div>
					<div className="firstLane">
						<NavLink to="/freebtc" className="navLink freeMenuItem" activeClassName="selectedTab" onClick={() => setPlayNavVisibility(false)}>{t.freeBTC}</NavLink>
					</div>				
					<div className="firstLane">
						<NavLink to="/rating" className="navLink ratingMenuItem" activeClassName="selectedTab">{t.rating}</NavLink>
					</div>
				</div>
			</div>
			<div className="right">
				<div className="curs right__item">
					<img className="curs__img" src="/images/btc-ico.svg" alt="btc"/>
					<div className="curs__value">${priceBTC}</div>
				</div>
				<div className="header__balance right__item" onClick={() => dispatch(switchCurrency())}>
					{balanceValue} <span className="btcFont2 colorTxt-1">{currency}</span>
				</div>
				<div className={`header__menu-wrap header__menu ${mobNavVisibility ? 'header__menu_close' : ''}`}
				     onClick={() => setMobNavVisibility(!mobNavVisibility)}/>
				<div className="right__item lang-wrap">
					<span onClick={() => setOpen(!open)}>
						{langNow === 'en' ? <EnImg/> : <RuImg/>}
					</span>
					{open &&
					<div className="lang-menu">
						<span onClick={() => setLang('en')}><EnImg/></span>
						<span onClick={() => setLang('ru')}><RuImg/></span>
					</div>
					}
				</div>
				
				{isAuthenticated && info.avatar_number !== 'undefined' &&
				<Link to="/profile">
					<img className="userAvaSmall" src={`/images/avatars/${info.avatar_number}.svg`} alt="ava"/>
				</Link>
				}
				{isAuthenticated &&
				<div className="logoutBut" onClick={() => dispatch(logOut())}>
					<div className="logoutPic"/>
				</div>
				}
				{!isAuthenticated &&
				<div className="btn-std" onClick={() => dispatch(showPopup('logIn'))}>{t.signIn}</div>
				}

			</div>
			{mobNavVisibility &&
			<div className="header_open_menu_mob mobileMenuClass pa">
				<Link to="/" className="mobileMenuString M-24-500-24"
				      onClick={() => setMobNavVisibility(false)}>{t.footer}</Link>
				<Link to="/moreless" className="mobileMenuString M-24-500-24"
				      onClick={() => setMobNavVisibility(false)}>{t.moreLess}</Link>
				<Link to="/freebtc" className="mobileMenuString M-24-500-24"
				      onClick={() => setMobNavVisibility(false)}>{t.freeBTC}</Link>
				<Link to="/rating" className="mobileMenuString M-24-500-24"
				      onClick={() => setMobNavVisibility(false)}>{t.rating}</Link>
			</div>
			}
		</div>
	);
}

export default Header;
