import React, {useEffect, useState} from 'react';
import {secondsToHms} from '../../utils/format';
import {useSelector} from "react-redux";

function CountdownEarn(props) {
	const {nextGameTime} = props;
	const nowTime = Math.floor(Date.now() / 1000);
	let timeLeftSec = nextGameTime - nowTime;
	if (timeLeftSec < 0) timeLeftSec = 0;

	const percentStart = Math.floor(timeLeftSec / (24 * 60 * 60) * 100);
	const time = secondsToHms(timeLeftSec);

	const [timeLeft, SetTimeLeft] = useState(timeLeftSec);
	const [timeLeftStr, SetTimeLeftStr] = useState(time);
	const [percent, SetPercent] = useState(percentStart);

	useEffect(() => {
		const int = setTimeout(() => {
			if (timeLeft > 0) {
				const newTimeLeft = timeLeft - 1;
				const newTimeLeftStr = secondsToHms(newTimeLeft);
				const percentNew = Math.floor(timeLeftSec / (24 * 60 * 60) * 100);

				SetTimeLeft(newTimeLeft);
				SetTimeLeftStr(newTimeLeftStr);
				SetPercent(percentNew);
			}
		}, 1000);

		return () => clearTimeout(int);
	},[timeLeft]);

	const t = useSelector(state => state.i18n.translations[state.i18n.locale]);

	return (
		<div className="card-row">
			<div className="countdown-back"/>
			<div id="countdownValve" className="countdown" style={{width: percent + '%'}}/>
			<div className="start">{t.start}</div>
			<div id="timerValve" className="timer">{timeLeftStr}</div>
		</div>
	)
}

export default CountdownEarn;