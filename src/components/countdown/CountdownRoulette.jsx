import React from 'react';
import {gameTypesValue} from "../../config";
import {useSelector} from "react-redux";
import {secondsToDhms} from "../../utils/format";

function CountdownRoulette(props) {
	const {timeCreated, gameType} = props;
	let timeLeftSec;
	let gameTypeTime = gameTypesValue[gameType];

	if (timeCreated) {
		const nowTime = Math.floor(Date.now() / 1000);
		timeLeftSec = timeCreated + gameTypeTime - nowTime;
		if (timeLeftSec < 0) timeLeftSec = 0;
	}

	const percent = (timeLeftSec / gameTypeTime) * 100;

	const t = useSelector(state => state.i18n.translations[state.i18n.locale]);

	return (
		<div className="countdown-div">
			<div className="countdown-back"/>
			<div className="countdown" style={{width: percent + '%'}}/>
			<div className="start">{timeLeftSec ? t.start : ''}</div>
			<div className="timer">{timeLeftSec ? secondsToDhms(timeLeftSec) + 's' : ''}</div>
			{!timeCreated && <div className="start textOnCountdown">{t.waitBets}</div>}
		</div>
	)
}

export default CountdownRoulette;