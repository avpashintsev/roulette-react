import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {showPopup, hidePopup} from "../../store/popups/actions";
import {logIn} from "../../store/auth/actions";
import Loader from "../../components/ui/Loader";
import validateInput from "../../utils/validate";

function LogIn() {
	const dispatch = useDispatch();

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [errors, setErrors] = useState({});
	const [loading, setLoading] = useState(false);

	const t = useSelector(state => state.i18n.translations[state.i18n.locale]);

	const [passwordShown, setPasswordShown] = useState(false);
	const togglePasswordVisiblity = () => {
	  setPasswordShown(passwordShown ? false : true);
	};

	const isValid = () => {
		const data = {email, password};

		const {errors, isValid} = validateInput(data);

		if (!isValid) {
			setErrors(errors);
		}

		return isValid;
	};

	const logInRequest = () => {
		if (!loading && isValid()) {
			setLoading(true);
			setErrors({});

			const userData = {
				email,
				password,
			};

			dispatch(logIn(userData)).then(() => {
				setLoading(false);
			});
		}
	};

	return (
		<div id="loginDiv" className="popup-fade">
			<div className="loginDiv">
				<div className="closePic" onClick={() => {
					dispatch(hidePopup('logIn'))
				}}/>
				<div className="signUp M-24-500-24 left-40">
						<span id="signUp" className="cursPointer colorTxt-4"
						      onClick={
							      () => {
								      dispatch(hidePopup('logIn'));
								      dispatch(showPopup('signUp'));
							      }
						      }>{t.signUp}</span>
					<span id="signIn" className="ml62">{t.signIn}</span></div>
				<div id="signInPanel">
					<div
						className={`input-group signInEmail left-40 signUpTxtArea ${errors.hasOwnProperty('email') ? 'input-group_error' : ''}`}>
						<input type="email" name="signUpEmail" id="signUpEmail"
						       defaultValue={email}
						       onChange={e => setEmail(e.target.value)}
						       className="depo_value signUpTxtArea" placeholder={t.email}/>

						<div className="input-group__error">{t[errors.email]}</div>
					</div>
					<div
						className={`input-group signUpPass left-40 signUpTxtArea ${errors.hasOwnProperty('password') ? 'input-group_error' : ''}`}>
						<input type={passwordShown ? "text" : "password"} name="signUpPass" id="signUpPass"
						       defaultValue={password}
						       onChange={e => setPassword(e.target.value)}
						       className="depo_value  signUpTxtArea " placeholder={t.password}/>
							   <i onClick={togglePasswordVisiblity} className={passwordShown ? "eyeOpen" : "eyeClosed"}></i>

						<div className="input-group__error">{t[errors.password]}</div>
					</div>
					<div className="getNewPass left-40 M-14-400-24 colorTxt-6" onClick={() => {
						dispatch(hidePopup('logIn'));
						dispatch(showPopup('forgotPass'));
					}}>Forgot your password?</div>
				</div>
				<div id="signInButton" className="button-2 left-40 signInBtnPos" onClick={logInRequest}>
					{loading ? <Loader/> : t.logIn}					
				</div>
				<div id="loginVK" class="socialLogin signInBtnPos logLeft-1 cursPointer">
            		<div class="vk"></div>
				</div>
				<div id="loginFB" class="socialLogin signInBtnPos logLeft-2 cursPointer">
					<div class="fbook fbigPos"></div>
				</div>
				<div id="loginIG" class="socialLogin signInBtnPos logLeft-3 cursPointer">
					<div class="igram fbigPos"></div>
				</div>
			</div>
		</div>
	);
}

export default LogIn;
