import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {showPopup, hidePopup} from "../../store/popups/actions";
import {changePasswordComplete} from "../../store/auth/actions";

function ForgotPassComplete() {
	const dispatch = useDispatch();

	const [password, setPassword] = useState('');

	const t = useSelector(state => state.i18n.translations[state.i18n.locale]);
	const passwordResetToken = useSelector(state => state.authReducer.passwordResetToken);
	const [passwordShown, setPasswordShown] = useState(false);
	const togglePasswordVisiblity = () => {
	  setPasswordShown(passwordShown ? false : true);
	};
	const action = () => {
		const data = {
			new_password: password,
			token: passwordResetToken,
		};

		dispatch(changePasswordComplete(data));
		dispatch(hidePopup('passwordRecoveryComplete'));
	};

	return (
		<div id="forgotPass" className="popup-fade">
			<div className="loginDiv forgotPass">
				<div className="closePic" onClick={() => {
					dispatch(hidePopup('passwordRecoveryComplete'))
				}}/>
				<div className="signUp M-24-500-24 left-40">{t.forgot}</div>
				<input type={passwordShown ? "text" : "password"} name="forgotPass" id="forgotPass" value={password}
				       onChange={e => setPassword(e.target.value)}
				       className="depo_value signInEmail signUpTxtArea left-40" placeholder={t.newPassword}/>
				<i onClick={togglePasswordVisiblity} className={passwordShown ? "eyeOpen" : "eyeClosed"}
				style={{ top: '100px', right: '50px' }}></i>
				<div id="forgotPassButton" className="button-2 left-40 signInBtnPos" onClick={action}>{t.changeA}</div>
			</div>
		</div>
	);
}

export default ForgotPassComplete;
