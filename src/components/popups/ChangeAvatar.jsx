import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {hidePopup} from "../../store/popups/actions";
import {updateAvatar} from "../../store/auth/actions";

function ChangeAvatar() {
	const dispatch = useDispatch();

	const t = useSelector(state => state.i18n.translations[state.i18n.locale]);

	const changeAvatar = (num) => {
		dispatch(hidePopup('changeAvatar'));
		dispatch(updateAvatar(num));
	};

	const avatarsElements = [];

	for (let i = 0; i < 20; i++) {
		avatarsElements.push(
			<img className="ava" src={`/images/avatars/${i}.svg`} alt="ava" key={i} onClick={() => changeAvatar(i)}/>
		);
	}

	return (
		<div id="confirmEmail" className="popup-fade">
			<div className="loginDiv newAvaPopup top-30">
				<div className="closePic" onClick={() => {
					dispatch(hidePopup('changeAvatar'))
				}}/>
				<div className="signUp M-24-500-24 left-40">{t.changeAvatar}</div>
				<div className="avatars">
					{avatarsElements}
				</div>
			</div>
		</div>
	);
}

export default ChangeAvatar;
