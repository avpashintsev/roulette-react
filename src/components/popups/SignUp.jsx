import React, {useState, useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {showPopup, hidePopup} from "../../store/popups/actions";
import validateInput from '../../utils/validate';
import {ReCaptcha, loadReCaptcha} from 'react-recaptcha-v3';
import {reCaptchaSiteKey} from '../../config';
import {signUp} from "../../store/auth/actions";
import Loader from "../../components/ui/Loader";

function SignUp() {
	const dispatch = useDispatch();
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [confirmPassword, setConfirmPassword] = useState('');
	const [agree, setAgree] = useState(true);
	const [recaptcha, setRecaptcha] = useState('');
	const [errors, setErrors] = useState({});
	const [loading, setLoading] = useState(false);

	const t = useSelector(state => state.i18n.translations[state.i18n.locale]);

	const [passwordShown, setPasswordShown] = useState(false);
	const togglePasswordVisiblity = () => {
	  setPasswordShown(passwordShown ? false : true);
	};
	const [passwordCShown, setPasswordCShown] = useState(false);
	const togglePasswordCVisiblity = () => {
	  setPasswordCShown(passwordCShown ? false : true);
	};
/*
	useEffect(() => {
		let isSubscribed = true;

		if(isSubscribed) loadReCaptcha(reCaptchaSiteKey);

		return () => isSubscribed = false;
	}, []);
*/
	const isValid = () => {
		const data = {agree, email, password, confirmPassword};

		const {errors, isValid} = validateInput(data);

		if (!isValid) {
			setErrors(errors);
		}

		return isValid;
	};

	const signUpRequest = () => {
		if (!loading && isValid()) {
			setLoading(true);
			setErrors({});
			const referralParentKey = localStorage.getItem('ref') ? localStorage.getItem('ref') : '';

			const userData = {
				email,
				password,
				referralParentKey,
				recaptcha,
			};

			dispatch(signUp(userData));
		}
	};

	return (
		<div id="loginDiv" className="popup-fade">
			<div className="loginDiv loginDiv2">
				<div className="closePic" onClick={() => {
					dispatch(hidePopup('signUp'))
				}}/>
				<div className="signUp M-24-500-24 left-40">
					<span id="signUp" className="">{t.signUp}</span>
					<span id="signIn" className="ml62 cursPointer colorTxt-4"
					      onClick={
						      () => {
							      dispatch(hidePopup('signUp'));
							      dispatch(showPopup('logIn'));
						      }
					      }>{t.signIn}</span></div>
				<div id="signUpPanel" className="">
					<div
						className={`input-group signInEmail left-40 signUpTxtArea ${errors.hasOwnProperty('email') ? 'input-group_error' : ''}`}>
						<input type="email" name="signUpEmail" id="signUpEmail"
						       defaultValue={email}
						       onChange={e => setEmail(e.target.value)}
						       className="depo_value signUpTxtArea" placeholder={t.email}/>

						<div className="input-group__error">{t[errors.email]}</div>
					</div>
					<div
						className={`input-group signUpPass left-40 signUpTxtArea ${errors.hasOwnProperty('password') ? 'input-group_error' : ''}`}>
						<input type={passwordShown ? "text" : "password"} name="signUpPass" id="signUpPass"
						       defaultValue={password}
						       onChange={e => setPassword(e.target.value)}
						       className="depo_value  signUpTxtArea " placeholder={t.password}/>
						<i onClick={togglePasswordVisiblity} className={passwordShown ? "eyeOpen" : "eyeClosed"}></i>
						<div className="input-group__error">{t[errors.password]}</div>
					</div>
					<div
						className={`input-group signUpCPass left-40 signUpTxtArea ${errors.hasOwnProperty('confirmPassword') ? 'input-group_error' : ''}`}>
						<input type={passwordCShown ? "text" : "password"} name="signUpCPass" id="signUpCPass"
						       defaultValue={confirmPassword}
						       onChange={e => setConfirmPassword(e.target.value)}
						       className="depo_value  signUpTxtArea " placeholder={t.confirmPassword}/>
						<i onClick={togglePasswordCVisiblity} className={passwordCShown ? "eyeOpen" : "eyeClosed"}></i>
						<div className="input-group__error">{t[errors.confirmPassword]}</div>
					</div>
					<div id="signUpTxt" className="colorTxt-1 foottxt primTxtPos M-14-400-24 top-384 left-80 width-325">
						{t.confirm1} <a target="_blank" href="/license.html" className="c-orange">{t.confirm2}</a>
						{/*I confirm that I agree to the */}
					</div>
					{!agree && <div id="signUpPr" className="checkPr left-40 top-384" onClick={() => setAgree(true)}/>}
					{agree &&
					<div id="signUpPrPic" className="checkPr left-40 checkPrPic top-384" onClick={() => setAgree(false)}/>}
				</div>
				<div id="signInButton" className="button-2 left-40 signInBtnPos" onClick={signUpRequest}>
					{loading ? <Loader/> : t.signUp}
				</div>
				<div id="loginVK" class="socialLogin signInBtnPos logLeft-1 cursPointer">
            		<div class="vk"></div>
				</div>
				<div id="loginFB" class="socialLogin signInBtnPos logLeft-2 cursPointer">
					<div class="fbook fbigPos"></div>
				</div>
				<div id="loginIG" class="socialLogin signInBtnPos logLeft-3 cursPointer">
					<div class="igram fbigPos"></div>
				</div>
				<ReCaptcha sitekey={reCaptchaSiteKey} verifyCallback={setRecaptcha}/>
			</div>
		</div>
	);
}

export default SignUp;
