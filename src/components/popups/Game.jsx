import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {hidePopup} from "../../store/popups/actions";
import {numberNonExponential} from "../../utils/format";

function Game() {
	const dispatch = useDispatch();

	const t = useSelector(state => state.i18n.translations[state.i18n.locale]);
	const status = useSelector(state => state.rouletteReducer.status);
	const members = useSelector(state => state.rouletteReducer.members);

	const playerElements = status.members.map((m, i) => {
		return <li className="liPopup" key={i}>
			<div className="userRank li-user-win">{i + 1}</div>
			{m.avatar_number !== 'undefined' &&
			<img className="userAvaSmallCardGame" src={`/images/avatars/${m.avatar_number}.svg`} alt="ava"/>
			}
			<div className="user-name-card5 left-136">{m.nickname}</div>
			<div className="li-user-win winPopup">{m.amount}</div>
		</li>
	});

	// game_time: 1607419995
	// members: Array(8)
	// 0:
	// amount: 0
	// avatar_number: 0
	// nickname: "cbctoken"
	// user_id: 16
	return (
		<div id="gameDiv" className="popup-fade">
			<div className="loginDiv gameDiv top-30 posAbs">
				<div className="closePic" onClick={() => {
					dispatch(hidePopup('game'))
				}}/>
				<div className="signUp M-24-500-24 left-40">{t.game}</div>
				<div id="gameIdPopup" className="averta M-20-500-1-2 posAbs gameIdPopup colorTxt-1">#{status.id}</div>
				<div className="posAbs signInEmail M-14-500-16 left-40">{t.fair}</div>
				<div className="posAbs signInEmail M-14-500-16 left-148"><a id="fgameLink" href=""
				                                                            target="_blank">{status.hash}</a></div>
				{/*<div className="posAbs M-14-500-16 left-40 top-152">{t.mode}</div>*/}
				{/*<div className="posAbs M-14-500-16 left-138 colorTxt-1 top-152"><span*/}
				{/*	id="popupGameTime">{t.minute}</span>, <span*/}
				{/*	id="popupGameType">{t.max10}</span></div>*/}
				<div className="jack-div left-40 top-120">
					<div className="jack-value"><span id="jackPopup">{numberNonExponential(status.amount, true)}</span><span className="btcTxt2">BTC</span>
					</div>
					<div className="jack-txt">{t.total}</div>
				</div>
				<div className="posAbs playersPopup left-235 top-120">
					<div id="cntPlayersPopup" className="jack-value colorTxt-7">{members.length}</div>
					<div className="jack-txt colorTxt-7">{t.players}</div>
				</div>
				<div className="slots-div width-82 left-323 top-120">
					{status.members &&
					<div id="slotsPopup" className="cnt-slots">{status.members.length}</div>
					}
					<div className="jack-txt txt-slots">{t.slots}</div>
				</div>
				<ul className="ulPopup left-40">
					{playerElements}
					{/*<li className="liPopup liPopupFirst">*/}
					{/*	<div className="userRank li-user-win">1</div>*/}
					{/*	<div className="li-user-pic li-pic-pos-2 headPos-1"/>*/}
					{/*	<div className="user-name-card5 left-136">Emilee</div>*/}
					{/*	<div className="li-user-perc colorTxt-1 time-pos left-136">12:05:32</div>*/}
					{/*	<div className="li-user-win winPopup winPopupTop">$241</div>*/}
					{/*	<div className="li-user-perc colorTxt-1 time-pos popupWinChance">2,32%</div>*/}
					{/*<div className="cardText popupWinDiv">{t.winner}</div>*/}
					{/*</li>*/}
					{/*<li className="liPopup">*/}
					{/*	<div className="userRank li-user-win">2</div>*/}
					{/*	<div className="li-user-pic li-pic-pos-2 headPos-1"/>*/}
					{/*	<div className="user-name-card5 left-136">Naseema</div>*/}
					{/*	<div className="li-user-perc colorTxt-1 time-pos left-136">12:05:31</div>*/}
					{/*	<div className="li-user-win winPopup">$500</div>*/}
					{/*</li>*/}

				</ul>
			</div>
		</div>
	);
}

export default Game;
