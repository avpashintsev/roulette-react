import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {hidePopup} from "../../store/popups/actions";

function ConfirmEmail() {
	const dispatch = useDispatch();

	const t = useSelector(state => state.i18n.translations[state.i18n.locale]);

	return (
		<div id="confirmEmail" className="popup-fade">
			<div className="loginDiv confirmEmail top-30">
				<div className="closePic" onClick={() => {
					dispatch(hidePopup('confirmEmail'))
				}}/>
				<div className="signUp M-24-500-24 left-40">{t.confirmEmail}</div>
				<div className="left-40 M-14-400-24 colorTxt-1 top-109 posAbs">{t.registered}
				</div>
				<div id="forgotPassButton" className="button-2 left-40 signInBtnPos" onClick={() => {
					dispatch(hidePopup('confirmEmail'))
				}}>{t.gotIt}
				</div>
			</div>
		</div>
	);
}

export default ConfirmEmail;
