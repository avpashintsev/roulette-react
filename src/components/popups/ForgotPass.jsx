import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {showPopup, hidePopup} from "../../store/popups/actions";
import {logIn, recoverRequest} from "../../store/auth/actions";

function ForgotPass() {
	const dispatch = useDispatch();

	const [sent, setSent] = useState(false);
	const [email, setEmail] = useState('');

	const t = useSelector(state => state.i18n.translations[state.i18n.locale]);

	const recover = () => {
		setSent(true);
		dispatch(recoverRequest(email));
	};

	return (
		<div id="forgotPass" className="popup-fade">
			<div className="loginDiv forgotPass">
				<div className="closePic" onClick={() => {
					dispatch(hidePopup('forgotPass'))
				}}/>
				<div className="signUp M-24-500-24 left-40">{t.forgot}</div>
				<input type="email" name="forgotPassEmail" id="forgotPassEmail" value={email} onChange={e => setEmail(e.target.value)}
				       className="depo_value signInEmail signUpTxtArea left-40" placeholder={t.email}/>
				{sent && <div className="recovery">{t.recovery}</div>}
				<div id="forgotPassButton" className="button-2 left-40 signInBtnPos" onClick={recover}>{t.submit}</div>
				<div className="M-14-400-24 colorTxt-1 forgotTxtPos">
						<span id="forgotSignIn" className="cursPointer forgotSignIn"
						      onClick={() => {
							      dispatch(hidePopup('forgotPass'));
							      dispatch(showPopup('logIn'));
						      }}
						>{t.signIn}</span>
					<span id="forgotSignUp" className="cursPointer forgotSignIn ml56"
					      onClick={() => {
						      dispatch(hidePopup('forgotPass'));
						      dispatch(showPopup('signUp'));
					      }}
					>{t.signUp}</span></div>
			</div>
		</div>
	);
}

export default ForgotPass;
