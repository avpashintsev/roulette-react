import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {hidePopup, showErrorPopup} from "../../store/popups/actions";
import {addAddress} from "../../store/auth/actions";
import WAValidator from 'wallet-address-validator';

function AddAddress() {
	const dispatch = useDispatch();

	const [address, setAddress] = useState('');

	const t = useSelector(state => state.i18n.translations[state.i18n.locale]);

	const action = () => {
		if (!WAValidator.validate(address, 'BTC')) {
			dispatch(showErrorPopup(t.notValid));
		} else {

			let data = {
				address,
				currency: "btc",
			};

			dispatch(addAddress(data));
		}

	};

	return (
		<div id="loginDiv" className="popup-fade">
			<div className="loginDiv loginDiv_addadress">
				<div className="closePic" onClick={() => {
					dispatch(hidePopup('addAddress'))
				}}/>
				<div className="signUp M-24-500-24 left-40" onClick={() => dispatch(hidePopup('addAddress'))}>
					{t.addBTCWallet}
				</div>
				<div id="signInPanel">
					<div
						className={`input-group signUpPass left-40 signUpTxtArea`}>
						<input type="text" name="signUpPass" id="signUpPass"
						       onChange={e => setAddress(e.target.value)}
						       className="depo_value  signUpTxtArea " placeholder={t.btcWallet}/>

					</div>
				</div>
				<div id="signInButton" className="button-2 left-40 signInBtnPos" onClick={action}>
					{t.submit}
				</div>
			</div>
		</div>
	);
}

export default AddAddress;