import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {hidePopup} from "../../store/popups/actions";

function Error() {
	const dispatch = useDispatch();

	const text = useSelector(state => state.popupReducer.successPopup.text);
	const t = useSelector(state => state.i18n.translations[state.i18n.locale]);

	return (
		<div id="forgotPass" className="popup-fade">
			<div className="loginDiv forgotPass">
				<div className="closePic" onClick={() => {
					dispatch(hidePopup('successPopup'))
				}}/>
				<div className="signUp M-24-500-24 left-40">{t.success}</div>
				<div className='signInEmail signUpTxtArea left-40 pa'>{text}</div>
				<div id="forgotPassButton" className="button-2 left-40 signInBtnPos" onClick={() => {
					dispatch(hidePopup('successPopup'));
				}}>{t.ok}</div>
			</div>
		</div>
	);
}

export default Error;
