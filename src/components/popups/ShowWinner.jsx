import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {hidePopup} from "../../store/popups/actions";
import {formatNumber, formattingDate} from "../../utils/format";

function ShowWinner() {
	const dispatch = useDispatch();

	const t = useSelector(state => state.i18n.translations[state.i18n.locale]);
	const winner = useSelector(state => state.rouletteReducer.winner);

	return (
		<div id="confirmEmail" className="popup-fade">
			<div className="loginDiv showWinnerPopup top-30">
				<div className="closePic" onClick={() => {
					dispatch(hidePopup('showWinner'))
				}}/>
				{winner.finish_time &&
				<div className="signUp M-24-500-24 left-40">
					Game: {winner.id}
					<span className="showWinnerPopup__date">{formattingDate(winner.finish_time)}</span>
				</div>
				}
				{winner.winner &&
				<div className="winner">
					<div className="winner__title">{t.winner}:</div>
					<div className="winner__ava">
						<img className="userAvaSmall" src={`/images/avatars/${winner.winner.avatar_number}.svg`} alt="ava"/>
						<div>nickname: {winner.winner.nickname}</div>
					</div>
					<div className="winner__value">{formatNumber(winner.amount)} {winner.currency}</div>
				</div>
				}
			</div>
		</div>
	);
}

export default ShowWinner;
