import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {hidePopup} from "../../store/popups/actions";

function BetInfo() {
	const dispatch = useDispatch();

	const t = useSelector(state => state.i18n.translations[state.i18n.locale]);

	return (
		<div id="betInfo" className="popup-fade">
			<div className="loginDiv forgotPass top-30">
				<div className="closePic" onClick={() => {
					dispatch(hidePopup('betInfo'))
				}}/>
				<div className="signUp M-24-500-24 left-40">{t.info}</div>
				<div id="closeInfoBut" className="button-2 left-40 signInBtnPos" onClick={() => {
					dispatch(hidePopup('betInfo'))
				}}>{t.gotIt}
				</div>
				<div className="left-40 M-14-400-24 colorTxt-1 posAbs betInfo">
					{t.getTicketDesc}
					<br/><br/>
					{t.getTicketDesc2}
				</div>
			</div>
		</div>
	);
}

export default BetInfo;
