import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getStats} from "../store/rating/actions";
import isEmpty from "lodash/isEmpty";
import {priceBackConvertAndRound} from "../utils/format";

function Stats() {
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(getStats());
	}, []);

	const stats = useSelector(state => state.ratingReducer.stats);
	const t = useSelector(state => state.i18n.translations[state.i18n.locale]);
	const price = useSelector(state => state.priceReducer.price);
	const currency = useSelector(state => state.priceReducer.currency);

	const isFetched = !isEmpty(stats);
	let games_today, max_bet, online_users, paid_total, top_jackpot, top_luck;

	if (isFetched) {
		games_today = stats.games_today;
		max_bet = stats.max_bet;
		online_users = stats.online_users;
		paid_total = stats.paid_total;
		top_jackpot = stats.top_jackpot;
		top_luck = stats.top_luck * 100;
	}

	return (
		<div className="stats">
			{isFetched &&
			<>
				<div className="card-1 left1">
					<div className="picClass pic-1"/>
					<div className="kost-cur">{currency}</div>
					<div id="topJack" className="cardValueTxt cvleft-2">{priceBackConvertAndRound(top_jackpot, price, currency)}</div>
					<div className="cardText colorTxt-1">{t.statsJackpot}</div>
				</div>
				<div className="card-1 left2">
					<div className="picClass pic-2"/>
					<div className="kost-cur">{currency}</div>
					<div id="paidTot" className="cardValueTxt cvleft-2">{priceBackConvertAndRound(paid_total, price, currency)}</div>
					<div className="cardText colorTxt-1">{t.statsPaid}</div>
				</div>
				<div className="card-1 left3-2">
					<div className="picClass pic-3"/>
					<div id="gameTod" className="cardValueTxt cvleft-2">{games_today}</div>
					<div className="cardText colorTxt-1">{t.statsGames}</div>
				</div>
				<div className="card-1 left4-2">
					<div className="picClass pic-4"/>
					<div id="topLuck" className="cardValueTxt cvleft-2">{parseInt(top_luck)}<span className="unit unit3321">%</span>
					</div>
					<div className="cardText colorTxt-1">{t.statsLuck}</div>
				</div>
				<div className="card-1 left5-2">
					<div className="picClass pic-5"/>
					<div className="kost-cur">{currency}</div>
					<div id="maxBet" className="cardValueTxt cvleft-2">{priceBackConvertAndRound(max_bet, price, currency)}</div>
					<div className="cardText colorTxt-1">{t.statsMaxBet}</div>
				</div>
				<div className="card-1 left6-2">
					<div className="picClass pic-6"/>
					<div id="onUse" className="cardValueTxt cvleft-2">{online_users}</div>
					<div className="cardText colorTxt-1"><div className="onlinePulse"></div>{t.statsUsers}</div>
				</div>
			</>
			}
		</div>
	);
}

export default Stats;
