import * as React from 'react';
import { DataGrid } from '@material-ui/data-grid';
import { useDemoData } from '@material-ui/x-grid-data-generator';

function PaymentProof() {
	const { data } = useDemoData({
		dataSet: 'Commodity',
		rowLength: 1000,
		maxColumns: 6,
	  });


	return (
		<div className="freebtc">
			<div id="pname" class="headerTxt">Payment proof</div>
			<div className="card-row"><div id="cardMakeBet-3" className="card card-license">
    <div style={{ height: 400, margin:'20px' }}>
      <DataGrid pagination {...data} className="dgMUI"/>
    </div>

		</div></div></div>
	);
}

export default PaymentProof