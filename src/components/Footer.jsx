import React from 'react';
import {useSelector} from "react-redux";
import { NavLink } from 'react-router-dom';

function Footer() {
	const t = useSelector(state => state.i18n.translations[state.i18n.locale]);

	return (
		<footer>
			<a href="/" className="footer-link">&#169; {t.footer}</a>
			<a href="/license.html" className="footer-link">{t.confirm2}</a>
<NavLink to="licenseagreement" className="footer-link">{t.confirm2}</NavLink>
<NavLink to="paymentproof" className="footer-link">{"payment proof"}</NavLink>
<NavLink to="faq" className="footer-link">{"faq"}</NavLink>
		</footer>
	);
}

export default Footer;
