import React from 'react';
import {useSelector} from "react-redux";
import Question from '../components/Question'



const FAQ=()=>{
const text = useSelector(state => state.i18n.translations[state.i18n.locale]);
const questions = text.FAQ
console.log(questions)


return(
    <div className="freebtc">
        <div id="pname" class="headerTxt">FAQ</div>
        <div className="card-row">
            <div >
                {questions.map(q=><Question answer={q.answer} question={q.question}/>)}
            </div>
        </div>
    </div>)
}

export default FAQ