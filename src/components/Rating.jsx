import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getRating} from "../store/rating/actions";
import isEmpty from "lodash/isEmpty";

function getJSX(users, param) {
	return users.map((user, key) => {
		const nickname = user.nickname;
		let value = user[param];
		if(param === 'chance') value = value * 100 + ' %';

		return <li className="li-card-6" key={key}>
			<div className="ratingNum2 rating-pos-1">{key + 1}</div>
			<div className="ratingUserPic2"/>
			<div className="ratingNum2 rating-pos-2">{nickname}</div>
			{param !== 'win' && <div className="ratingNum2 rating-pos-3 colorTxt-1">{parseInt(value)}{param === 'chance' ? '%' : ''}</div>}
		</li>;
	});
}

function Rating() {
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(getRating());
	}, []);

	const rating = useSelector(state => state.ratingReducer.rating);
	const t = useSelector(state => state.i18n.translations[state.i18n.locale]);

	const isFetched = !isEmpty(rating);
	let max_games, top_jackpot, top_luck;

	if (isFetched) {
		top_jackpot = getJSX(rating.top_jackpot, 'win');
		max_games = getJSX(rating.max_games, 'games');
		top_luck = getJSX(rating.top_luck, 'chance');
	}

	return (
		<div className="rating">
			{isFetched &&
			<div className="card-row">
				<div className="card-big">
					<div className="picClass pic-1 card-6-pic-pos"/>
					<div className="gameTxt card-6-pos">{t.topJackpot}</div>
					<ul className="ul-card-6">{top_jackpot}</ul>
				</div>
				<div className="card-big">
					<div className="picClass pic-3 card-6-pic-pos"/>
					<div className="gameTxt card-6-pos">{t.maxGames}</div>
					<ul className="ul-card-6">{max_games}</ul>
				</div>
				<div className="card-big">
					<div className="picClass pic-4 card-6-pic-pos"/>
					<div className="gameTxt card-6-pos">{t.topLuck}</div>
					<ul className="ul-card-6">{top_luck}</ul>
				</div>
			</div>
			}
		</div>
	);
}

export default Rating;
