import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import PersonalCard from './PersonalCard';
import isEmpty from "lodash/isEmpty";
import copy from 'copy-to-clipboard';
import {getAddress, removeAddress, updatePasswords, updateUserData} from '../store/auth/actions';
import {getReferrals} from '../store/referrals/actions';
import {showErrorPopup, showPopup} from '../store/popups/actions';
import {formatNumber, priceBackConvertAndRound} from "../utils/format";

function Profile() {
	const [nickname, setNickname] = useState('');
	const [phone_number, setPhoneNumber] = useState('');
	const [avatar_number, setAvatarNumber] = useState('');
	const [referralLink, setReferralLink] = useState('');
	const [loading, setLoading] = useState(false);

	const [currentPassword, setCurrentPassword] = useState('');
	const [newPassword, setNewPassword] = useState('');
	const [newPasswordConf, setNewPasswordConf] = useState('');

	const dispatch = useDispatch();

	const info = useSelector(state => state.authReducer.info);
	const statistics = useSelector(state => state.authReducer.info.statistics);
	const address = useSelector(state => state.authReducer.address);
	const referralCount = useSelector(state => state.referralReducer.count);
	const t = useSelector(state => state.i18n.translations[state.i18n.locale]);
	const price = useSelector(state => state.priceReducer.price);
	const currency = useSelector(state => state.priceReducer.currency);

	useEffect(() => {
		if (!isEmpty(info) && !loading) {
			setLoading(true);
			setReferralLink(window.location.origin + '?ref=' + info.referralKey);
			setNickname(info.nickname);
			setPhoneNumber(info.phone_number);
			setAvatarNumber(info.avatar_number);
		}
	}, [info]);

	useEffect(() => {
		dispatch(getReferrals());
		dispatch(getAddress());
	}, []);

	const updateInfo = () => {
		let data = {
			nickname,
			phone_number,
			avatar_number,
		};

		dispatch(updateUserData(data));
	};

	const updatePassword = () => {
		if (newPassword !== newPasswordConf) {

			dispatch(showErrorPopup(t.passwordsMustMatch));

		} else {

			let data = {
				current_password: currentPassword,
				new_password: newPassword,
			};

			dispatch(updatePasswords(data));
		}
	};

	const addressElements = address.map((addr, i) => {
		return (
			<li className="li-card-Wal" key={i}>
				<div className="walCircle">
					<div className="psBack-4 bitPicOrange"/>
				</div>
				<div className="user-name-card5 tabletStyleWal">{addr.address}</div>
				{/*<div className="txt-1 averta colorTxt-1 walBalPos">1.00340019 BTC</div>*/}
				{/*<div className="psBack-4 walletSet"/>*/}
				<div className="psBack-4 walletDel" onClick={() => {
					dispatch(removeAddress(addr.id))
				}}/>
			</li>
		);
	});

	return (
		<div className="profile">
			<div className="card-row">
				<div className="card-mixed">
					<div className="card-mixed-1">
						<PersonalCard deposit={true} withdrawal={true} changeAva={true}/>
					</div>
					<div className="card-mixed-2">
						<div className="gameTxt">{t.referral}</div>
						<div id="refCnt" className="M-16-500-1-75 averta refCntPos colorTxt-1">{referralCount}</div>
						<div className="colorTxt-1 foottxt refTxtPos">{t.registerReferral}</div>
						<div className="colorTxt-4 M-13-500-1-23 refLinkTxtPos left-40">{t.referralLink}</div>
						<div className="depo_value ref_link colorTxt-4">{referralLink}</div>
						<div className="ref_pic cursPointer" onClick={() => copy(referralLink)}/>
					</div>
				</div>
				<div className="card-mixed">
					<div className="card-mixed-3">
						<div className="gameTxt">{t.primary}</div>
						<div className="colorTxt-4 M-13-500-1-23 userEmailTxtTop left-40">{t.email}</div>
						<input type="text" name="userEmail" id="userEmail" className="depo_value colorTxt-5 userEmail"
						       placeholder="email" defaultValue={info.email} disabled/>
						<div className="colorTxt-4 M-13-500-1-23 userNameTxtTop left-40">{t.nickname}</div>
						<input type="text" name="userNameZ" id="userNameZ" className="depo_value colorTxt-5 userNameZ"
						       defaultValue={nickname} onChange={e => setNickname(e.target.value)}/>
						<div className="colorTxt-4 M-13-500-1-23 userPhoneTxtTop left-40">{t.phone}</div>
						<input type="tel" name="userPhone" id="userPhone" className="depo_value colorTxt-5 userPhone"
						       defaultValue={phone_number} pattern="\+[0-9 ()]+" onChange={e => setPhoneNumber(e.target.value)}/>
						<div id="saveData" className="button-2 cubeLeft-1 bottom-24" onClick={updateInfo}>{t.save}</div>
					</div>
				</div>
				<div className="card-mixed">
					<div className="card-mixed-4">
						<div className="gameTxt">{t.changePassword}</div>
						<div className="colorTxt-4 M-13-500-1-23 userEmailTxtTop left-40">{t.currentPassword}</div>
						<input type="password" name="userPass" id="userPass" className="depo_value colorTxt-5 userEmail"
						       placeholder={t.currentPassword} onChange={e => setCurrentPassword(e.target.value)}/>
						<div className="colorTxt-4 M-13-500-1-23 userNameTxtTop left-40">{t.newPassword}</div>
						<input type="password" name="userPassNew" id="userPassNew" className="depo_value colorTxt-5 userNameZ"
						       placeholder={t.newPassword} onChange={e => setNewPassword(e.target.value)}/>
						<div className="colorTxt-4 M-13-500-1-23 userPhoneTxtTop left-40">{t.confirmPassword}</div>
						<input type="password" name="userPassAppr" id="userPassAppr" className="depo_value colorTxt-5 userPhone"
						       placeholder={t.confirmPassword} onChange={e => setNewPasswordConf(e.target.value)}/>
						<div id="savePasData" className="button-2 cubeLeft-1 bottom-24" onClick={updatePassword}>{t.save}</div>
					</div>
					{/*<div className="card-mixed-5">*/}
					{/*	<div id="Slip" className="leftSlip bradius-1 slipLeft-1 animated"/>*/}
					{/*	<div id="leftSlip" className="leftSlip bradius-2 slipLeft-1 txt-1 line-height-big animatedTxtColor">BTC*/}
					{/*	</div>*/}
					{/*	<div id="rightSlip"*/}
					{/*	     className="leftSlip bradius-3 slipLeft-2 txt-1 line-height-big disable-color animatedTxtColor">USD*/}
					{/*	</div>*/}
					{/*</div>*/}
				</div>
			</div>

			<div className="card-row">
				<div className="card-wallet">
					<div className="gameTxt">{t.btcWallets}</div>
					<div id="addWallet" className="addWallet M-14-500-2-71" onClick={() => {
						dispatch(showPopup('addAddress'))
					}}>
						{t.add}
						<div className="minus plus addPos"/>
					</div>
					<ul className="ul-card-Wal">{addressElements}</ul>
				</div>
			</div>
			{statistics &&
			<div className="card-row">
				<div className="card-stats">
					<div id="UserGameTot" className="card-8">
						<div className="picClass pic-3"/>
						<div id="UserGameTotal" className="cardValueTxt cardValueTxtEX cardValueTxtBig cvleft-2">{statistics.gamesTotal ? statistics.gamesTotal : 0}</div>
						<div className="cardText colorTxt-1">GAMES TOTAL</div>
					</div>
					<div id="UserPaidTot" className="card-8 left2">
						<div className="picClass pic-2"/>
						<div className="kost-cur">{currency}</div>
						<div id="UserPaidTotal" className="cardValueTxt cardValueTxtEX cardValueTxtBig cvleft-2">
							{priceBackConvertAndRound(statistics.totalPaid, price, currency)}
						</div>
						<div className="cardText colorTxt-1">PAID TOTAL</div>
					</div>
					<div id="UserWonTot" className="card-8 left3-2">
						<div className="picClass pic-2"/>
						{/*<div className="unit unitEX unit-left-1">$</div>*/}
						<div id="UserWonTotal" className="cardValueTxt cardValueTxtEX cardValueTxtBig cvleft-1">{statistics.winTotal}</div>
						<div className="cardText colorTxt-1">WON TOTAL</div>
					</div>
					<div id="UserTopLuck" className="card-8 left4-2">
						<div className="picClass pic-4"/>
						<div id="topLuck" className="cardValueTxt cardValueTxtEX cardValueTxtBig cvleft-2">{statistics.topLuck.toFixed(2)}<span
							className="unit unit23">%</span>
						</div>
						<div className="cardText colorTxt-1">TOP LUCK</div>
					</div>
				</div>
			</div>
			}
		</div>
	);
}

export default Profile;
