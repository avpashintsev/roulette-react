import React from 'react';
import './index.css';
const { createRef , Component } = React;

export class Slots extends Component {
  static slotItems = (()=> {
      let nums = [];
      for (let index = 0; index < 10; index++) {
        nums.push(index);       
      }
      return nums;
    })();

  constructor(props) {
    super(props);

    this.state = {};

    for (let i = 0; i < Slots.slotItems.length; i++) {
      this.state[`item${i + 1}`] = Slots.slotItems[i];
    }
    this.state['rolling'] = false;
    // get ref of dic onn which elements will roll
    this.slotRef = [createRef(), createRef(), createRef(), createRef()];
  }
  componentDidMount() {
    if(this.props.number) this.roll();
  }

  // to trigger roolling and maintain state
  roll = () => {
    this.setState({
      rolling: true
    });
    setTimeout(() => {
      this.setState({ rolling: false });
    }, 700);

    // looping through all slots to start rolling
    this.slotRef.forEach((slot, i) => {
      // this will trigger rolling effect
      const number = ("" + this.props.number).split('')[i];
      const selected = this.triggerSlotRotation(slot.current, number);
      // this.setState({ [`item${i + 1}`]: selected });
      this.setState({ [`item${i + 1}`]: number });
    });

  };

  // this will create a rolling effect and return random selected option
  triggerSlotRotation = (ref, randomOption) => {
    function setTop(top) {
      ref.style.top = `${top}px`;
    }
    let options = ref.children;
    // let randomOption = Math.floor(
    //   Math.random() * Slots.slotItems.length
    // );
    let choosenOption = options[randomOption];
    setTop(-choosenOption.offsetTop + 2);
    return Slots.slotItems[randomOption];
  };

  render() {
    return (
      <div className="SlotMachine">
       <SlotBox items={Slots.slotItems} ref={this.slotRef[0]}/>
       <SlotBox items={Slots.slotItems} ref={this.slotRef[1]}/>
       <SlotBox items={Slots.slotItems} ref={this.slotRef[2]}/>
       <SlotBox items={Slots.slotItems} ref={this.slotRef[3]}/>
        {/*<div*/}
        {/*  className={!this.state.rolling ? "roll rolling" : "roll"}*/}
        {/*  // onClick={!this.state.rolling && this.roll}*/}
        {/*  onClick={!this.state.rolling ? this.roll : undefined}*/}
        {/*  disabled={this.state.rolling}*/}
        {/*>*/}
        {/*  {this.state.rolling ? "Rolling..." : "ROLL"}*/}
        {/*</div>*/}
      </div>
    );
  }
}

export const SlotBox = React.forwardRef(({items}, ref) => (  
      <div className="slot">
      <section className="cube cubeLeft-1">
        <div className="container" ref={ref}>
          {items.map((item, i) => (
            <div key={i}>
              <span>{item}</span>
            </div>
          ))}
        </div>
      </section>
    </div>
));