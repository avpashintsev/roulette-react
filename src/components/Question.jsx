import React from 'react';

const Question=({question, answer})=>{
    return<div className="card">
        <p>answer: {answer}</p>
        <p>question {question}</p>
    </div>
}

export default Question