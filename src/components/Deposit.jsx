import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {getUserDepositWallets} from '../store/auth/actions';
import QRCode from 'qrcode.react';
import Promocode from './Promocode';

function Deposit() {
	const dispatch = useDispatch();

	const isAuthenticated = useSelector(state => state.authReducer.isAuthenticated);
	const wallets = useSelector(state => state.authReducer.wallets);
	const t = useSelector(state => state.i18n.translations[state.i18n.locale]);

	useEffect(() => {
		if(isAuthenticated) dispatch(getUserDepositWallets());
	}, [isAuthenticated]);

	return (
		<div className="main-frame widthMini deposit-wrap">
			<Promocode/>
			<div className="deposit">
				<div id="pname" className="headerTxt">{t.deposit}</div>
				{isAuthenticated &&
				<div className="card-row">
					<div className="card-block">
						<div className="refill-title">{t.personalWallet}</div>
						<div className="refill-desc">{t.refill}</div>
						{wallets.BTC &&
						<div>
							<div className="refill-wallet">{wallets.BTC}</div>
							<QRCode value={wallets.BTC} bgColor="transparent" fgColor="#fff" size={208}/>
						</div>
						}
					</div>
				</div>
				}
			</div>
		</div>
	);
}

export default Deposit;
