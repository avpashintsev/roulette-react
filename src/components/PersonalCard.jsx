import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import {useDispatch, useSelector} from "react-redux";
import isEmpty from 'lodash/isEmpty';
import {numberNonExponential} from "../utils/format";
import {logIn, logOut, signUp} from "../store/auth/actions";
import {hidePopup, showPopup} from "../store/popups/actions";

function PersonalCard({deposit, withdrawal,logout}) {
	let nickname;

	const dispatch = useDispatch();

	const t = useSelector(state => state.i18n.translations[state.i18n.locale]);
	const info = useSelector(state => state.authReducer.info);
	const isAuthenticated = useSelector(state => state.authReducer.isAuthenticated);
	const priceBTC = useSelector(state => state.priceReducer.price);
	const currency = useSelector(state => state.priceReducer.currency);

	const [balance, setBalance] = useState('');
	if (!isEmpty(info)) {
		nickname = info.nickname;
	}else{
		nickname = "Your are guest"
	}

	useEffect(() => {
		if (!isEmpty(info)) {
			const balanceBTC = numberNonExponential(info.balance.BTC, true);
			const balanceUSD = (priceBTC * balanceBTC).toFixed(2);

			if (currency === 'BTC') {
				setBalance(balanceBTC)
			} else {
				setBalance(balanceUSD)
			}
		}
	}, [currency, info, priceBTC]);

	return (
		<div className="card personal-card">
			{info.avatar_number !== 'undefined' &&
			<img className="userAva" src={`/images/avatars/${info.avatar_number}.svg`} alt="ava"
			     onClick={() => dispatch(showPopup('changeAvatar'))}/>
			}
			<div className="userName">{nickname}</div>
			<div className="userBalance">{balance ? `${balance}  ${currency}` : ''}</div>
			{deposit && isAuthenticated &&
			<Link to="/deposit">
				<div className="button-1">{t.deposit}</div>
			</Link>
			}
			{withdrawal && isAuthenticated &&
			<Link to="/withdrawal">
				<div className="button-1 button-1EX">{t.withdraw}</div>
			</Link>
			}
			{logout && isAuthenticated &&
			<div className="logoutBut" onClick={() => dispatch(logOut())}>
				<div className="logoutPic"/>
			</div>
			}
			{!isAuthenticated &&
			
			<div class="btn-std">Sign In</div>
			}
		</div>
	);
}

export default PersonalCard;
