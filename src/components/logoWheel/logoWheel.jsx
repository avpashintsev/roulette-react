import React from 'react'
import styles from './logoWheel.module.css';

export const LogoWheel=()=>{
    const slots = [32,15,19,4,21,2,25,17,34,6,27,13,36,11,30,8,23,10,5,24,16,33,1,20,14,31,9,22,18,29,7,28,12,35,3,26,0]
    return <div className={styles.btcAnimatedLogo}>
      <div className={styles.btcLogoWheel}></div>
      <div className={styles.plate}>
        <ul className={styles.inner}>
            {slots.map((n)=>
                <li className={styles.number}></li>
            )
            }
        </ul>
      </div>
</div>
}

export default LogoWheel;