import React, {useEffect, useState} from 'react';
import PersonalCard from './PersonalCard';
import {useDispatch, useSelector} from "react-redux";
import isEmpty from "lodash/isEmpty";
import {formatNumber, formattingDate, priceConvertAndRound} from "../utils/format";
import {initGame, runEtheroll, getEtherollHistory} from "../store/etheroll/actions";
import Loader from "./ui/Loader";

function Moreless() {
	const dispatch = useDispatch();

	const [value, setValue] = useState(0.0001);
	const [percent, setPercent] = useState(50);
	const [loading, setLoading] = useState(false);
	const [loadingHistory, setLoadingHistory] = useState(false);
	const [onlyMyHistory, setOnlyMyHistory] = useState(false);

	const isAuthenticated = useSelector(state => state.authReducer.isAuthenticated);
	const init = useSelector(state => state.etherollReducer.init);
	const finish = useSelector(state => state.etherollReducer.finish);
	const history = useSelector(state => state.etherollReducer.history);
	const t = useSelector(state => state.i18n.translations[state.i18n.locale]);
	let proof, gameId, isWin, winNumber, gameFinished, historyElements;
	const possibleProfit = parseFloat((value * Number((100 / percent * (1 - 2 / 100)).toFixed(4))).toFixed(8));

	const price = useSelector(state => state.priceReducer.price);
	const currency = useSelector(state => state.priceReducer.currency);

	if (!isEmpty(init)) {
		gameId = init.gameId;
		proof = init.proof;
		gameFinished = false;
	}

	if (!isEmpty(finish)) {
		gameFinished = true;
		isWin = finish.isWin;
		winNumber = finish.winNumber;
		// winSecret = finish.winSecret;
	}

	const setBetValue = val => {
		val = parseFloat(parseFloat(val).toFixed(8));
		if (val < 0.00000001) val = 0.00000001;
		setValue(formatNumber(val));
	};

	useEffect(() => {
		if (isAuthenticated) {
			dispatch(initGame());
			dispatch(getEtherollHistory());
		}
	}, [isAuthenticated]);

	const action = () => {
		setLoading(true);

		const data = {
			currency: 'BTC',
			amount: value,
			user_number: percent,
			game_id: init.gameId,
		};

		dispatch(runEtheroll(data)).then(() => {
			setLoading(false);
			getHistory(1, onlyMyHistory);
		});
	};

	const reset = () => {
		setLoading(true);

		dispatch(initGame()).then(() => {
			setLoading(false);
		});
	};

	const getHistory = (onlyUser = false) => {
		setLoadingHistory(true);
		setOnlyMyHistory(onlyUser);

		dispatch(getEtherollHistory(1, onlyUser)).then(() => {
			setLoadingHistory(false);
		});
	};

	if (!isEmpty(history) && !loadingHistory) {
		historyElements = history.games.map(game => {
			return <li className="li-card-7" key={game.id}>
				<div className="ratingNum ghPos-2">{game.id}</div>
				<div className="ratingNum ghPos-3 colorTxt-1 ghHeadTabletTop1">
					{priceConvertAndRound(game.amount, price, currency)} {currency}</div>
				<div className="ratingNum ghPos-4 colorTxt-1 ghHeadTabletTop2">{game.user_number}%</div>
				<div className="ratingNum ghPos-5 ghHeadTabletTop3">
					{game.is_win ? <span className='colorTxt-2'>{t.win}</span> : <span className='colorTxt-3'>{t.lose}</span>}
				</div>
				<div className="ratingNum ghPos-6 colorTxt-1 ghHeadTabletTop4">
					{priceConvertAndRound(game.potential_reward, price, currency)} {currency}</div>
				<div className="ratingNum ghPos-7 colorTxt-1 ghHeadTabletTop5">{game.win_number}</div>
				<div className="ratingNum ghPos-8 colorTxt-1 ghHeadTabletTop6">{formattingDate(game.created_at)}</div>
				<div className="ratingNum ghPos-9 colorTxt-1 ghHeadTabletTop7">{game.win_secret}</div>
			</li>;
		});
	} else {
		historyElements = <Loader/>
	}

	return (
		<div className="moreless">
			<div className="card-row">
				<PersonalCard deposit={true} withdrawal={true}/>
				<div className="card card-bet">
					<div id="input-div" className="input-div">
						<div id="bet_minus" className="minus-but">
							<div className="minus" onClick={() => setBetValue(parseFloat((parseFloat(value) - 0.0001)).toFixed(8))}/>
						</div>
						<input type="text" name="bet_result" className="input-bet"
						       value={value}
						       onChange={e => {
							       setBetValue(e.target.value)
						       }}
						/>
						<div id="bet_plus" className="minus-but plus-but">
							<div className="minus plus" onClick={() => setBetValue(parseFloat((parseFloat(value) + 0.0001)).toFixed(8))}/>
						</div>
					</div>
					<div id="winTxt" className="winChance">{t.probability}</div>
					<div id="winChancePerc" className="winChancePerc colorTxt-1">{percent}%</div>
					<div id="input-bet-div-2" className="input-bet-div-2"/>
					<div id="input-bet-div" className="input-bet-div"
					     style={{width: `calc((100% - 48px) * ${percent / 100 * 4 / 3})`}}/>
					<div id="input-bet-div-3" className="input-bet-div-3"/>
					<input type="range" min="1" max="75"
					       defaultValue={percent}
					       onChange={e => setPercent(parseInt(e.target.value))}
					       className="slider"/>
					{gameFinished &&
					<div id="makeBet-2" className="button-2 bet-pos-7 fe-pulse-w-pause" onClick={reset}>{loading ? <Loader/> : t.newGame}</div>
					}
					{!gameFinished &&
					<div id="makeBet-2" className="button-2 bet-pos-7 fe-pulse-w-pause" onClick={action}>{loading ? <Loader/> : t.makeAbet}</div>
					}
					<div className="btcTxt colorTxt-1">BTC</div>
				</div>
				<div className="card card-info">
					<div id="openSlots">
						<div className="gameTxt">Game</div>
						<div id="gameId" className="gameId colorTxt-1">{gameId}</div>
						<div className="fgameTxt">{t.fairGame}</div>
						<a id="fgameIdHREF" href="">
							<div id="fgameId" className="fgameId colorTxt-1">{proof}</div>
						</a>
						<div className="slots-div actionFinal">
							{gameFinished &&
							<div>
								<div className='win'>{isWin ? t.win : t.lose}</div>
								<div className='winSub'>{t.winNumber}: {winNumber}</div>
							</div>
							}
							{!gameFinished &&
							<div>
								<div className="percent-k"><span className="percent-k__value">{percent}</span> %</div>
								<div className="percent-k__chance">{t.chance}</div>
							</div>
							}
						</div>
					</div>
					<div id="jack-div" className="jack-div">
						<div className="jack-value"><span id="jack-value">{possibleProfit}</span><span
							className="btcTxt2">BTC</span>
						</div>
						<div className="jack-txt">{t.possibleWin}</div>
					</div>
				</div>
			</div>

			{isAuthenticated &&
			<div className="card-block">
				<div className="table-head">
					<div className="table-head__title">{t.gamesHistory}</div>

					<div className="table-head-nav">
						<div className={`table-head-nav__item ${onlyMyHistory ? '' : 'table-head-nav__item_active'}`}
						     onClick={getHistory}>{t.allPlayers}
						</div>
						<div className={`table-head-nav__item ${onlyMyHistory ? 'table-head-nav__item_active' : ''}`}
						     onClick={() => getHistory(true)}>{t.onlyMe}
						</div>
					</div>
				</div>
				<div className="li-card-7 thead">
					<span className="pos headPos-1">{t.game} #</span>
					<span className="pos headPos-2">{t.bet}</span>
					<span className="pos headPos-3">{t.chance}</span>
					<span className="pos headPos-4">{t.result}</span>
					<span className="pos headPos-5">{t.possibleWin}</span>
					<span className="pos headPos-6">{t.win}</span>
					<span className="pos headPos-7">{t.date}</span>
					<span className="pos headPos-8">{t.fair}</span>
				</div>
				<ul className="ul-card-7">
					{historyElements}
				</ul>
			</div>
			}
		</div>
	);
}

export default Moreless;
