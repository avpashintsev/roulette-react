import React from 'react'

function Loader() {
	return (
		<div className='spinner'>
			<div className='bounce1'/>
			<div className='bounce2'/>
			<div/>
		</div>
	)
}

export default Loader
