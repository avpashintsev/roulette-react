import React, {useState} from 'react';
import {useSelector} from "react-redux";

const Dropdown = props => {
	const t = useSelector(state => state.i18n.translations[state.i18n.locale]);

	const {options, onSetOption} = props;
	const [open, setOpen] = useState(false);
	const [title, setTitle] = useState(t[options[0].labelId]);
	const [titleActive, setTitleActive] = useState(true);

	const optionElements = options.map((option, i) => {
		return <div className="dropdown-option" key={i}
		            onClick={() => {
			            setTitle(t[option.labelId]);
			            setOpen(false);
			            setTitleActive(true);
			            onSetOption(option.value);
		            }}>{(t[option.labelId])}</div>
	});

	return (
		<div className="dropdown-wrap">
			<div
				className={`dropdown-card ${open ? 'dropdown-card_active' : ''} ${titleActive ? 'dropdown-option_active' : ''}`}
				onClick={() => {
					setOpen(!open)
				}}>
				<div className="betValue colorTxt-5">{title}</div>
				<div className="arrow-div pos5"/>
			</div>
			{open &&
			<div className="dropdown-options">{optionElements}</div>
			}
		</div>
	);
};

export default Dropdown;