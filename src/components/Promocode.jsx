import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import {useDispatch, useSelector} from "react-redux";
import isEmpty from 'lodash/isEmpty';
import {numberNonExponential} from "../utils/format";
import {getAddress} from "../store/auth/actions";
function Promocode(){
	const dispatch = useDispatch();
    let [balanceValue, setBalanceValue] = useState('');
    const info = useSelector(state => state.authReducer.info);
	useEffect(() => {
		if (!isEmpty(info)) {
			let balanceBTC = numberNonExponential(info.balance.BTC, true);

			setBalanceValue(balanceBTC)

		}
    }, [info]);
    useEffect(() => {
		dispatch(getAddress());
	}, []);
	return (
		<div className="card personal-card">
            <div className="colorTxt-1 foottxt promocodeText">Use promocode to refill your balance</div>
            <input className="depo_value colorTxt-5"></input>

            <div id="saveData" className="button-2 cubeLeft-1 bottom-24" onClick={() => setBalanceValue(+balanceValue+10)}>Apply</div>
 			{balanceValue}           
		</div>
	);
}

export default Promocode;
