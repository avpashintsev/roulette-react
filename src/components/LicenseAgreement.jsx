import React, {useEffect, useState} from 'react';


function LicenseAgreement() {
	return (
		<div className="freebtc">
			<div className="card-row">
				<div id="cardMakeBet-3" className="card card-license">
				<p><span>END USER LICENSE AGREEMENT</span></p>

<p><span>This end user license
agreement (the "Agreement") should be read by you (the
"User" or "you") in its entirety prior to your use of Bitcoin
Roulette service or products. By using gamebitco.in (Site) you consent to the
terms and conditions set forth in this Agreement as it may be updated or
modified from time to time in accordance with the provisions below and therein.
</span></p>

<p><span>1. GRANT OF
LICENSE/INTELLECTUAL PROPERTY </span></p>

<p><span>The terms "Bitcoin
Roulette", the domain name "Gamebitco.in" and any other trade marks, service marks, signs, trade names and/or
domain names used by Bitcoin Roulette on the Site from time to time (the
"Trade Marks"), are the trade marks,
service marks, signs, trade names and/or domain names of Bitcoin Roulette, and
these entities reserve all rights to such Trade Marks. In addition, all content
on the Site, including, but not limited to, images, pictures, graphics,
photographs, animations, videos, music, audio and text (the "Site
Content") belongs to Bitcoin Roulette and is protected by copyright and/or
other intellectual property or other rights. You hereby acknowledge that by
using the Service and the Site you obtain no rights in the Site Content and/or
the Trade Marks, or any part thereof. Under no circumstances may you use the
Site Content and/or the Trade Marks without Bitcoin Roulette' prior written
consent. Additionally, you agree not to do anything that will harm or
potentially harm the rights, including the intellectual property rights, held
by Bitcoin Roulette, in, the Trade Marks or the Site Content nor will you do
anything that damages the image or reputation of Bitcoin Roulette, its,
employees, directors, officers and consultants. </span></p>

<p><span>2. NO WARRANTIES. </span></p>

<p><span>2.1. Bitcoin Roulette
disclaims any and all warranties, expressed or implied, in connection with the
Service which is provided to you "AS IS" and we provide you with no
warranty or representation whatsoever regarding its quality, fitness for
purpose, completeness or accuracy.</span></p>

<p><span>2.2. Regardless of our
efforts to provide you with service of the highest quality, safety and
security, we make no warranty that the Service will be uninterrupted, timely or
error-free, that defects will be corrected or that the Site shall be free from
viruses, bugs or other contaminants.</span></p>

<p><span>2.3. Bitcoin Roulette
reserves the right to suspend, discontinue, modify, remove or add to the
Service in its absolute discretion with immediate effect and without an
obligation to provide you with notice where we consider it necessary to do so,
including (for example) where we receive information that you have entered into
any self-exclusion agreement with any gambling provider or where we deem it
necessary for the management, maintenance or update and we shall not be liable
in any way whatsoever for any loss suffered as a consequence of any decision
made by Bitcoin Roulette in this regard. </span></p>

<p><span>3. AUTHORITY </span></p>

<p><span>Bitcoin Roulette retains
authority over the issuing, maintenance, and closing of Users' accounts on the
Site. The decision of Bitcoin Roulette' management, as regards any aspect of a
User's account, use of the Service, or dispute resolution, is final and shall
not be open to review or appeal. </span></p>

<p><span>4. YOUR REPRESENTATIONS AND
WARRANTIES </span></p>

<p><span>Prior to your use of the
Service and on an ongoing basis you represent, warrant, covenant and agree
that: </span></p>

<p><span>4.1. there is a risk of
losing money when using the Service and that Bitcoin Roulette has no
responsibility to you for any such loss;</span></p>

<p><span>4.2. your use of the
Service is at your sole option, discretion and risk;</span></p>

<p><span>4.3. you are aged 18 or
over and that you are not currently self-excluded from any online or mobile
gambling site and that you will inform us immediately if you enter into a
self-exclusion agreement with any gambling provider. </span></p>

<p><span>5. PROHIBITED USES </span></p>

<p><span>5.1. EXTERNAL PLAYER
ASSISTANCE PROGRAMS (EPA). Bitcoin Roulette prohibits those External Player
Assistance Programs ("EPA Programs") which are designed to provide an
"Unfair Advantage" to players. Bitcoin Roulette defines
"External" to mean computer software, and non-software-based
databases or profiles (e.g. web sites and subscription services).</span></p>

<p><span>5.2. FRAUDULENT BEHAVIOUR.
In the event that Bitcoin Roulette deems that a User has engaged or attempted
to engage in fraudulent, unlawful, dishonest or improper activity while using
the Service, including without limitation, engaging in any of the activities
set forth in this clause 5 or any other game manipulation, or the making of any
fraudulent payment, including without limitation, Bitcoin Roulette shall be
entitled to take such action as it sees fit, including, but not limited to:<br/>
immediately blocking a User's access to the Service;<br/>
terminating a User's account with Bitcoin Roulette;<br/>
seizing the funds within a User's account; </span></p>

<p><span>6. BREACH </span></p>

<p><span>6.1. Without prejudice to
any other rights, if a User breaches in whole or in part any provision
contained herein, Bitcoin Roulette reserves the right to take such action as it
sees fit, including terminating this Agreement or any other agreement in place
with the User, immediately blocking the User's access to the Service,
terminating such User's account on the Site, seizing all monies held in the
User's account on the Site and/or taking legal action against such User.</span></p>

<p><span>6.2. You agree to fully
indemnify, defend and hold harmless Bitcoin Roulette and its shareholders,
directors and employees from and against all claims, demands, liabilities,
damages, losses, costs and expenses, including legal fees and any other charges
whatsoever, howsoever caused, that may arise as a result of:<br/>
your breach of this Agreement, in whole or in part;<br/>
violation by you of any law or any third party rights; <br/>
and use by you of the Service or use by any other person accessing the Service
using your Login Credentials (as defined below) , whether or not with your
authorization. </span></p>

<p><span>7. LIMITATION OF LIABILITY.
</span></p>

<p><span>7.1. Under no
circumstances, including negligence, shall Bitcoin Roulette be liable for any
special, incidental, direct, indirect or consequential damages whatsoever
(including, without limitation, damages for loss of business profits, business
interruption, loss of business information, or any other pecuniary loss)
arising out of the use (or misuse) of the Service even if Bitcoin Roulette had
prior knowledge of the possibility of such damages.</span></p>

<p><span>7.2. Nothing in this
Agreement shall exclude or limit Bitcoin Roulette' liability for: (a) death or
personal injury resulting from its negligence; or (b) fraud or fraudulent
misrepresentation. </span></p>

<p><span>8. SECURITY AND YOUR
ACCOUNT </span></p>

<p><span>8.1. Each User account
shall be accessible through the special unique uniform resource locator (URL)
provided to him/her at the first visit. </span></p>

<p><span>8.2. The User agrees that he/she
is solely responsible for all use of the Service under his/her URL and that
he/she shall not disclose the URL to any person whatsoever nor permit another
person to use the Service via his/her User account.</span></p>

<p><span>8.3. The User is obliged to
keep his/her URL secret and confidential at all times and to take all efforts
to protect their secrecy and confidentiality. </span></p>

<p><span>8.4. Please note that
monies held in your Bitcoin Roulette account do not accrue interest.</span></p>

<p><span>8.5. You will not be able
to place any bets using the Service in an amount greater than the total amount
of money in your Bitcoin Roulette account.</span></p>

<p><span>8.6. You are fully
responsible for paying all monies owed to Bitcoin Roulette. You agree not to
make any chargebacks, and/or deny or reverse any payment made by you in respect
of the Service. You will reimburse Bitcoin Roulette for any chargebacks, denial
or reversal of payments you make and any loss suffered by us as a consequence. </span></p>

<p><span>9. AMENDMENT </span></p>

<p><span>Bitcoin Roulette reserves
the right to update or modify this Agreement or any part thereof at any time
without notice and you will be bound by such amended Agreement within 14 days
of it being posted at the Site. Therefore, we encourage you to visit the Site
regularly and check the terms and conditions contained in the version of the
Agreement in force at such time. </span></p>

<p><span>10. SEVERABILITY </span></p>

<p><span>If a provision of this
Agreement is or becomes illegal, invalid or unenforceable in any jurisdiction,
that shall not affect the validity or enforceability in that jurisdiction of
any other provision hereof or the validity or enforceability in other
jurisdictions of that or any other provision hereof. </span></p>

<p><span>11. ASSIGNMENT </span></p>

<p><span>Bitcoin Roulette reserves
the right to assign this agreement, in whole or in part, at any time without
notice. The User may not assign any of his/her rights or obligations under this
Agreement. </span></p>







				</div>
			</div>
		</div>
	);
}

export default LicenseAgreement;
