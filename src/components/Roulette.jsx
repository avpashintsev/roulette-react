import React, {useEffect, useState} from 'react';
import PersonalCard from './PersonalCard';
import {useDispatch, useSelector} from "react-redux";
import {showPopup} from "../store/popups/actions";
import CountdownRoulette from "./countdown/CountdownRoulette";
import {formatNumber, formattingDate} from "../utils/format";
import {getRouletteInfo, getStatus} from "../store/roulette/actions";
import {getTicket} from "../store/roulette/actions";
import {getRouletteHistory} from "../store/roulette/actions";
import isEmpty from "lodash/isEmpty";
import Loader from "./ui/Loader";
import Dropdown from "./ui/Dropdown";
import {gameModes, gameTypes} from "../config";
import {priceBackConvertAndRound, priceConvertAndRound} from "../utils/format";

let getStatusInterval;

function Roulette() {
	const dispatch = useDispatch();

	const [value, setValue] = useState(1);
	const [loading, setLoading] = useState(false);
	const [loadingHistory, setLoadingHistory] = useState(false);
	const [onlyMyHistory, setOnlyMyHistory] = useState(false);
	const [gameMode, setGameMode] = useState(gameModes[0].value);
	const [gameType, setGameType] = useState(gameTypes[0].value);
	const [currentGameId, setCurrentGameId] = useState();
	const [needNewGameId, setNeedNewGameId] = useState(true);

	let historyElements, playersNowElements, membersElements, timeCreated;

	const isAuthenticated = useSelector(state => state.authReducer.isAuthenticated);
	const status = useSelector(state => state.rouletteReducer.status);
	const history = useSelector(state => state.rouletteReducer.history);
	const t = useSelector(state => state.i18n.translations[state.i18n.locale]);
	const members = useSelector(state => state.rouletteReducer.members);

	const price = useSelector(state => state.priceReducer.price);
	const currency = useSelector(state => state.priceReducer.currency);

const firstLineBets = [1,2,3]
const secondLineBets = [5,7,10]
const step = 1
/*долбит сервер чтобы узнать статус игры
	useEffect(() => {
		dispatch(getStatus(gameMode, gameType));
		clearInterval(getStatusInterval);

		getStatusInterval = setInterval(() => {
			dispatch(getStatus(gameMode, gameType));
		}, 1000);
	}, [gameMode, gameType]);
*/
	useEffect(() => {
		if (!needNewGameId && status && currentGameId && status.id !== currentGameId) {
			dispatch(getRouletteInfo(currentGameId));
			setCurrentGameId(status.id);
		}
	}, [status, currentGameId]);

	const setGameModeHandler = (value) => {
		setGameMode(value);
		setNeedNewGameId(true);
	};

	const setGameTypeHandler = (value) => {
		setGameType(value);
		setNeedNewGameId(true);
	};

	useEffect(() => {
		if (needNewGameId && status.id !== currentGameId) {
			setCurrentGameId(status.id);
			setNeedNewGameId(false);
		}
	}, [needNewGameId, status]);

	// useEffect(() => {
	// 	console.log('getHistory');
	// 	if (isAuthenticated) getHistory();
	// }, [isAuthenticated]);
	//
	// useEffect(() => {
	// 	dispatch(getRouletteHistory())
	// }, []);

	const makeBet = () => {
		setLoading(true);

		const data = {
			amount: value, //здесь нужна конвертация с текущей валюты в битки
			type: gameMode,
			duration: gameType,
		};

		dispatch(getTicket(data)).then(() => {
			dispatch(getStatus(gameMode, gameType));
			setLoading(false);
		});
	};

	const setBetValue = val => {
		val = parseFloat(parseFloat(val).toFixed(8));
		if (val < 0.00000001) val = 0.00000001;
		setValue(formatNumber(val));
	};

	const getHistory = (onlyUser = false) => {
		setLoadingHistory(true);
		setOnlyMyHistory(onlyUser);

		dispatch(getRouletteHistory(1, onlyUser)).then(() => {
			setLoadingHistory(false);
		});
	};

	if (!isEmpty(history) && !loadingHistory) {
		historyElements = history.games.map(game => {
			if (!game.amount) return '';

			return <li className="li-card-7" key={game.id}>
				<div className="ratingNum ghPos-2">{game.id}</div>
				<div className="ratingNum ghPos-3 colorTxt-1 ghHeadTabletTop1">
					{game.amount ? parseFloat(game.amount.toFixed(8)) : ''} {game.currency}
				</div>
				<div className="ratingNum ghPos-4 colorTxt-1 ghHeadTabletTop2">{game.users_count}</div>
				<div
					className="ratingNum ghPos-5 ghHeadTabletTop3">{game.winner ? parseInt(game.winner.chance.toFixed(2) * 100) + '%' : ''}</div>
				<div className="ratingNum ghPos-6 colorTxt-1 ghHeadTabletTop4">{game.winner ? game.winner.nickname : ''}</div>
				{/*<div className="ratingNum ghPos-7 colorTxt-1 ghHeadTabletTop5">{game.win_number}</div>*/}
				<div className="ratingNum ghPos-8 colorTxt-1 ghHeadTabletTop6">{formattingDate(game.created_at)}</div>
				<div className="ratingNum ghPos-9 colorTxt-1 ghHeadTabletTop7">{game.win_secret}</div>
			</li>;
		});
	} else {
		historyElements = <Loader/>
	}

	if (!isEmpty(status)) {
		const amount = status.amount;
		timeCreated = status.game_time;

		playersNowElements = status.members.map((member, i) => {
			const percent = parseInt((parseFloat(member.amount) / parseFloat(amount)) * 100);
			return <div className="players-now__item" key={i}>
				{member.avatar_number !== 'undefined' &&
				<img className="userAvaSmall" src={`/images/avatars/${member.avatar_number}.svg`} alt="ava"/>
				}
				<div className="player-name">{member.nickname}</div>
				<div className="player-percent colorTxt-1">{percent}%</div>
			</div>;
		});

		membersElements = status.members.map((member, i) => {
			return <li className="li-card-5" key={i}>
				{member.avatar_number !== 'undefined' &&
				<img className="userAvaSmallCard" src={`/images/avatars/${member.avatar_number}.svg`} alt="ava"/>
				}
				<div className="user-name-card5">{member.nickname}</div>
				<div className="li-user-win">{formatNumber(member.amount)} BTC</div>
			</li>
		});
	}

	return (
		<div className="roulette">

			<div className="card-row">
				<Dropdown placeholder="Game Mode" options={gameModes} onSetOption={setGameModeHandler}/>
				<Dropdown placeholder="Game Type" options={gameTypes} onSetOption={setGameTypeHandler}/>
				<CountdownRoulette timeCreated={timeCreated} gameType={gameType}/>
			</div>

			<div className="card-row">
				<PersonalCard deposit={true} withdrawal={true}/>
				<div className="card card-bet">
					<div id="input-div" className="input-div">
						<div id="bet_minus" className="minus-but">
							<div className="minus" onClick={() => setBetValue(parseFloat((parseFloat(value) - 
								priceBackConvertAndRound(step, price, currency,8)
								)))}/>
						</div>
						<input type="text" name="bet_result" className="input-bet"
						       value={priceBackConvertAndRound(value, price, currency,8)}
						       onChange={e => {
								   setBetValue(priceBackConvertAndRound(e.target.value, price, currency,8))
						       }}
						/>
						<div id="bet_plus" className="minus-but plus-but">
							<div className="minus plus"
								 onClick={() => setBetValue(parseFloat((parseFloat(value) +step
									//priceBackConvertAndRound(step, price, currency,9)
								 )))}/>
							</div>
					</div>
					<div className="buttons">
						<div className="buttons__row">
							{firstLineBets.map(bet=>
								<div className="buttons__btn" onClick={() => setBetValue(parseFloat(bet))}>
									{priceBackConvertAndRound(bet, price, currency,8)}
								</div>
								)}
						</div>
						<div className="buttons__row">
						{secondLineBets.map(bet=>
								<div className="buttons__btn" onClick={() => setBetValue(parseFloat(bet))}>
									{priceBackConvertAndRound(bet, price, currency,8)}
								</div>
								)}
						</div>
					</div>
					{/*<div className="newText start colorTxt-1">You can make 10 more bets<span id="maxCntBet"> or 0.1 BTC</span>*/}
					{/*</div>*/}
					<div id="makeBet" className="button-2 bet-pos-7 fe-pulse-w-pause" onClick={makeBet}>{t.makeAbet}</div>
					<div className="btcTxt colorTxt-1">{currency}</div>
					<div className="iconInfo cursPointer inf-pos-1" onClick={() => {
						dispatch(showPopup('betInfo'))
					}}/>
				</div>
				<div className="card card-game">
					<div id="openSlots">
						<div className="gameTxt">{t.game}</div>
						<div id="gameId" className="gameId colorTxt-1">{status.id}</div>
						<div className="iconInfo cursPointer inf-pos-2" onClick={() => {
							dispatch(showPopup('game'))
						}}/>
						<div className="fgameTxt">{t.fairGame}</div>
						<a id="fgameIdHREF" href="">
							<div id="fgameId" className="fgameId colorTxt-1">fd4rsjbrs5k4wdsbfksd34sfd</div>
						</a>
						<div className="slots-div">
							<div className="cnt-slots">
								<span id="cnt-slots">{members.length}</span>
							</div>
							<div className="jack-txt txt-slots">
								<span id="slotsTxt">{t.players}</span>
							</div>
						</div>
					</div>
					<div id="jack-div" className="jack-div">
						<div className="jack-value">
							<span id="jack-value">{status.amount ? 
							parseFloat(priceConvertAndRound(status.amount.toFixed(8), price, currency,2))
							 : ''}</span>
							<span className="btcTxt2">{currency}</span>
						</div>
						<div className="jack-txt">{t.total}</div>
					</div>
				</div>
			</div>

			{!isEmpty(status) && status.members.length > 0 &&
			<div className="card-row">
				<div className="ul-card-4 players-now">
					{playersNowElements}
				</div>
			</div>
			}

			<div className="card-row">
				<ul className="ul-card-4 ul-card-4-1 posAbs usersGame">
					{membersElements}
				</ul>
			</div>

			{/*{isAuthenticated &&*/}
			{/*<div className="card-block">*/}
			{/*	<div className="table-head">*/}
			{/*		<div className="table-head__title">Games History</div>*/}
			{/*	</div>*/}
			{/*	<div className="li-card-7 thead">*/}
			{/*		<span className="pos headPos-1">Game #</span>*/}
			{/*		<span className="pos headPos-2">Bet</span>*/}
			{/*		<span className="pos headPos-3">Players</span>*/}
			{/*		<span className="pos headPos-4">Chance</span>*/}
			{/*		<span className="pos headPos-5">Winner</span>*/}
			{/*		/!*<span className="pos headPos-6">Win</span>*!/*/}
			{/*		<span className="pos headPos-7">Date</span>*/}
			{/*		<span className="pos headPos-8">Fair Game</span>*/}
			{/*	</div>*/}
			{/*	<ul className="ul-card-7">*/}
			{/*		{historyElements}*/}
			{/*	</ul>*/}
			{/*</div>*/}
			{/*}*/}
		</div>
	);
}

export default Roulette;
