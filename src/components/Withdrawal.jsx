import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {withdrawal} from '../store/withdrawal/actions';
import validateInput from "../utils/validate";
import Loader from "./ui/Loader";
import {getAddress} from "../store/auth/actions";

function Withdrawal() {
	const dispatch = useDispatch();

	const [amount, setAmount] = useState(0.01);
	const [wallet, setWallet] = useState('');
	const [open, setOpen] = useState(false);
	const [errors, setErrors] = useState({});
	const [loading, setLoading] = useState(false);

	const address = useSelector(state => state.authReducer.address);
	const t = useSelector(state => state.i18n.translations[state.i18n.locale]);

	useEffect(() => {
		dispatch(getAddress());
	}, []);

	const isValid = () => {
		const data = {wallet};

		const {errors, isValid} = validateInput(data);

		if (!isValid) {
			setErrors(errors);
		}

		return isValid;
	};

	const withdrawalRequest = () => {
		if (!loading && isValid()) {
			setLoading(true);
			setErrors({});

			const data = {
				address: wallet,
				currency: 'BTC',
				amount,
			};

			dispatch(withdrawal(data)).then(() => {
				setLoading(false);
			});
		}
	};

	const addressElements = address.map((addr, i) => {
		return (
			<div className="firstLane M-16-500-1-5 paddingWal thirdLane walMenuString" key={i} onClick={() => {
				setWallet(addr.address);
				setOpen(false);
			}}>
				{addr.address}
			</div>
		);
	});

	return (
		<div className="main-frame widthMini deposit-wrap">
			<div className="deposit">
				<div id="pname" className="headerTxt">{t.withdraw}</div>
				<div className="depocard">
					<div className="M-13-500-1-23 colorTxt-4 depoPos-1">{t.amount}</div>
					<div id="withTxt" className="M-13-500-1-23 colorTxt-4">{t.btcWallet}</div>
					<div id="withdrawCard">
						<input type="text" name="withdrawInput" id="withdrawInput"
						       className="depo_value colorTxt-5 tabletWidth withdrawInput"
						       defaultValue={amount}
						       onChange={e => setAmount(e.target.value)}
						/>
						<div id="btcOnInput" className="playMenuText textOnInput colorTxt-1">BTC</div>

						{/*<div className={`input-group withdrawSelectWallet left-40 signUpTxtArea ${errors.hasOwnProperty('email') ? 'input-group_error' : ''}`}>*/}

						{/*	<input type="text" name="withdrawInput" id="withdrawInput"*/}
						{/*	       className="depo_value colorTxt-5 tabletWidth withdrawInput  M-16-500-1-5"*/}
						{/*	       defaultValue={wallet}*/}
						{/*	       onChange={e => setWallet(e.target.value)}*/}
						{/*	/>*/}
						{/*	<div className="input-group__error">{errors.wallet}</div>*/}
						{/*</div>*/}

						<div id="withdrawSelectWallet" className="withdrawInput withdrawSelectWallet M-16-500-1-5 paddingWal eee"
						onClick={() => setOpen(!open)}>
							<div id="withdrawSelectWalletTxt" className="withdrawSelectWalletTxt">{wallet}</div>
							<div className="arrow-div pos5"/>
						</div>

						<div className="M-13-500-1-23 colorTxt-4 depoPos-3 walTxtComm">Min 0.001 BTC<span className="ml24">Max 350 BTC</span><span
							className="commissionSpan">Commission 0.0005 BTC</span></div>

						{open &&
						<div id="withdrawCardMenu" className="withdrawCardMenu h144">{addressElements}</div>
						}
					</div>
					<div className="button-2 button-2 depoPos-4" onClick={withdrawalRequest}>
						{loading ? <Loader/> : t.withdrawFunds}
					</div>
				</div>
			</div>
		</div>
	);
}

export default Withdrawal;
