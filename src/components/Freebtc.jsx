import React, {useEffect, useState} from 'react';
import PersonalCard from './PersonalCard';
import {useDispatch, useSelector} from "react-redux";
import {getLastEarn, runNewEarn} from "../store/earn/actions";
import {Link} from 'react-router-dom';
import isEmpty from "lodash/isEmpty";
import {numberNonExponential, priceConvertAndRound} from "../utils/format";
import CountdownEarn from "../components/countdown/CountdownEarn";
import {Slots} from "./slots/slot";

function Freebtc() {
	const dispatch = useDispatch();
	const data = useSelector(state => state.earnReducer.data);

	const t = useSelector(state => state.i18n.translations[state.i18n.locale]);
	const price = useSelector(state => state.priceReducer.price);
	const currency = useSelector(state => state.priceReducer.currency);

	const [nextGameTime, setNextGameTime] = useState();
	const [winSum, setWinSum] = useState();
	const [winNumber, setWinNumber] = useState();
	const [formattedWinNumber, setFormattedWinNumber] = useState();

	useEffect(() => {
		dispatch(getLastEarn());
	}, []);

	useEffect(() => {
		if(!isEmpty(data)) {
			setNextGameTime(data.nextGameTime);
			setWinNumber(data.winNumber);
			setWinSum(numberNonExponential(data.winSum, true));
			setFormattedWinNumber(('0000' + data.winNumber).substr(-4));
		}
	}, [data]);

	return (
		<div className="freebtc">
			{nextGameTime && <CountdownEarn nextGameTime={nextGameTime}/>}
			<div className="card-row">
				<PersonalCard deposit={true} logout={true}/>
				<div id="cardMakeBet-3" className="card card-lucky">
					<div id="luckyNum" className="txt-1 txt-122 txt-top-1 txt-left-1">{t.luckyNumber}</div>
					<div className="txt-1 txt-122 txt-top-1 txt-left-2">{t.payout}</div>
					<div className="txt-1 txt-top-2 txt-left-1 txt-bold-500 colorTxt-1">0 - 9886</div>
					<div className="txt-1 txt-top-2 txt-left-2 txt-bold-500 colorTxt-1">{priceConvertAndRound(0.00000005, price, currency, 5)} {currency}</div>
					<div className="txt-1 txt-top-3 txt-left-1 txt-bold-500 colorTxt-1">9886 - 9985</div>
					<div className="txt-1 txt-top-3 txt-left-2 txt-bold-500 colorTxt-1">{priceConvertAndRound(0.00000186, price, currency, 4)} {currency}</div>
					<div className="txt-1 txt-top-4 txt-left-1 txt-bold-500 colorTxt-1">9886 - 9985</div>
					<div className="txt-1 txt-top-4 txt-left-2 txt-bold-500 colorTxt-1">{priceConvertAndRound(0.00001864, price, currency, 4)} {currency}</div>
					<div className="txt-1 txt-top-5 txt-left-1 txt-bold-500 colorTxt-1">9994 - 9997</div>
					<div className="txt-1 txt-top-5 txt-left-2 txt-bold-500 colorTxt-1">{priceConvertAndRound(0.00018637, price, currency, 4)} {currency}</div>
					<div className="txt-1 txt-top-6 txt-left-1 txt-bold-500 colorTxt-1">9997 - 9999</div>
					<div className="txt-1 txt-top-6 txt-left-2 txt-bold-500 colorTxt-1">{priceConvertAndRound(0.00186374, price, currency, 4)} {currency}</div>
					<div className="txt-1 txt-top-7 txt-left-1 txt-bold-500 colorTxt-1">10,000</div>
					<div className="txt-1 txt-top-7 txt-left-2 txt-bold-500 colorTxt-1">{priceConvertAndRound(0.01863735, price, currency, 4)} {currency}</div>
					<div className="button-2 bet-pos-8 txt-left-1 fe-pulse-w-pause" onClick={() => dispatch(runNewEarn())}>Roll</div>
					<Link to="/moreless" className="txt-1 txt-top-8 txt-right-1 txt-bold-500 colorTxt-1 cursPointer">
						{t.moreLess}
					</Link>
					<div className="arrow-div pos6 transform-arrow-270 cursPointer"/>
				</div>
				<div id="gameCard" className="card card-num">
					{winNumber && formattedWinNumber &&
						<div>
							<Slots number={formattedWinNumber}/>
							{/*<div id="cubes">*/}
							{/*	<div className="cube cubeLeft-1">{('' + formattedWinNumber)[0]}</div>*/}
							{/*	<div className="cube cubeLeft-2">{('' + formattedWinNumber)[1]}</div>*/}
							{/*	<div className="cube cubeLeft-3">{('' + formattedWinNumber)[2]}</div>*/}
							{/*	<div className="cube cubeLeft-4">{('' + formattedWinNumber)[3]}</div>*/}
							{/*</div>*/}
						</div>
					}
					{winSum &&
					<div id="jack-div" className="jack-div">
						<div className="jack-value"><span id="jack-value">
							{priceConvertAndRound(winSum, price, currency, 5)}</span>
							<span className="btcTxt2">{currency}</span>
						</div>
						<div className="jack-txt">{t.jackpot}</div>
					</div>
					}
				</div>
			</div>
		</div>
	);
}

export default Freebtc;
