import React from "react";

export const reCaptchaSiteKey = '6LfI2uMUAAAAAAvo3kcFU0kDpaXAB8QG1JCCCb-P';

export const gameModes = [
	{labelId: 'max10', label: '10 Max', value: 'max10'},
	{labelId: 'standard', label: 'Standard', value: 'default'},
];

export const gameTypes = [
	{labelId: 'minute', label: 'Minute', value: 'minute'},
	{labelId: 'day', label: 'Day', value: 'day'},
	{labelId: 'week', label: 'Week', value: 'week'},
	{labelId: 'month', label: 'Month', value: 'month'},
];

export const gameTypesValue = {
	minute: 60,
	day: 60 * 60 * 24,
	week: 60 * 60 * 24 * 7,
	month: 60 * 60 * 24 * 30,
};