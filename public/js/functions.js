let tick = 0;

window.onload = function() {
    let timerDiv = setInterval(() => countdown(), 100);
    
    $('.closePic').click(function() {
        $(this).parents('#loginDiv').hide();
        $(this).parents('#forgotPass').hide();
        $(this).parents('#confirmEmail').hide();
        $(this).parents('#gameDiv').hide();
        $(this).parents('#betInfo').hide();
        return false;
    });
    
    $(document).keydown(function(e) {
        if (e.keyCode == 27) {
            e.stopPropagation();
            $('#loginDiv').hide();
            $('#forgotPass').hide();
            $('#confirmEmail').hide();
            $('#gameDiv').hide();
            $('#betInfo').hide();
        }
    });
    
    $('#betInfo').click(function(e) {
        if ($(e.target).closest('.loginDiv').length == 0) $(this).hide();
    });
    
    $('#gameDiv').click(function(e) {
        if ($(e.target).closest('.loginDiv').length == 0) $(this).hide();
    });
    
    $('#confirmEmail').click(function(e) {
        if ($(e.target).closest('.loginDiv').length == 0) $(this).hide();
    });
    
    $('#forgotPass').click(function(e) {
        if ($(e.target).closest('.loginDiv').length == 0) $(this).hide();
    });
    
    $('#loginDiv').click(function(e) {
        if ($(e.target).closest('.loginDiv').length == 0) $(this).hide();
    });
    
    $('#signUp').click(function() {
        signHchange('signIn','signUp');
    });
    
    $('#signIn').click(function() {
        signHchange('signUp','signIn');
    });
    
    $('#signInButton').click(function() {
        $('#loginDiv').hide();
        $('#forgotPassEmail').html('');
        document.getElementById('forgotPassEmail').required = false;
        $('#forgotPass').show();
    });
    
    $('#forgotPassButton').click(function() {
        $('#forgotPass').hide();
        $('#confirmEmail').show();
    });
    
    $('#closeInfoBut').click(function() {
        $('#betInfo').hide();
    });
    
    $('#forgotSignIn').click(function() {
        $('#forgotPass').hide();
        $('#loginDiv').show();
    });
    
    $('#forgotSignUp').click(function() {
        $('#forgotPass').hide();
        $('#loginDiv').show();
        signHchange('signIn','signUp');
    });
};

function showMobileMenu() {
    let divMobile = document.getElementById('divMobile');
    if (divMobile.classList.contains('elemHide')) {
        $('#mobile-but').addClass('mobile-but-close');
        $('#divMobile').removeClass('elemHide');
        divMobile.style.zIndex = "9000";
        $('#openMenu').addClass('elemHide');
        $('#openMenuType').addClass('elemHide');
    } else {
        $('#divMobile').addClass('elemHide');
        $('#mobile-but').removeClass('mobile-but-close');
        divMobile.style.zIndex = "1";
        $('#openMenu').addClass('elemHide');
        $('#openMenuType').addClass('elemHide');
    }
}

function OpenStartPageEX(){
    if ($('.shitClass').width() == 328) {
        OpenStartPage('258px','1932px','2116px');
    }
	else if ($('.shitClass').width() == 540) {
        OpenStartPage('304px','1698px','1842px');
    }
    else {
        OpenStartPage('268px','1332px','1460px');
    }
}

function OpenStartPage(val1,val2,val3) {
    let divMobile = document.getElementById('divMobile');
    $('#divMobile').addClass('elemHide');
    $('#mobile-but').removeClass('mobile-but-close');
    divMobile.style.zIndex = "1";
    $('#main-div').show();
    $('#page1').show();
    $('#pageRating').hide();
    $('#gameTipe').show();
    $('#gameTime').show();
    $('#countdown-div').show();
    $('#valve').addClass('elemHide');
    $('#card-3').show();
    $('#logoutBut').removeClass('elemHide');
    $('#userWithdraw').addClass('elemHide');
    $('#cardMakeBet-1').show();
    $('#cardMakeBet-2').hide();
    $('#cardMakeBet-3').hide();
    $('#gameCard').show();
    $('#cubes').hide();
    $('#chanceNum').hide();
    $('#percTxt').hide();
    $('#all-slots').show();
    $('#openSlots').show();
    $('#cnt-slots').show();
    $('#slotsTxt').show();
    $('#cnahceTxt').hide();
    $('#card-4').show();
    $('#card-5').show();
    $('#card-7').hide();
    $('#referral').addClass('elemHide');
    $('#primary').addClass('elemHide');
    $('#chgpswrd').addClass('elemHide');
    $('#slipCard').addClass('elemHide');
    $('#walletCard').addClass('elemHide');
    $('.forScrollEX').addClass('elemHide');
    $('#deposit').addClass('elemHide');
    $('#openMenu').addClass('elemHide');
    $('#openMenuType').addClass('elemHide');
    $('#pageRating').hide();
    $('#playMenuOpen').addClass('elemHide');
    $('#playArrow').removeClass('transform-arrow');
    $('#jack-div').removeClass('jack-div-2');
	
	$('#card-3').removeClass('card3top2');
	$('#card-3').removeClass('card3top3');

	$('#footer').removeClass('footerTop2');
	$('#footer').removeClass('footerTop3');
	$('#footer').removeClass('footerTop4');
	$('#footer').removeClass('footerTop5');
	$('#footer').removeClass('footerTop6');

    $('#gameCard').removeClass('gameCardTop-2');
    $('#gameCard').removeClass('topValveGame');
    $('#userDeposit').removeClass('left-390');
}

function ratingEX() {
    if ($('.shitClass').width() == 1110) { 
        rating('822px','950px');
    }
    else if ($('.shitClass').width() == 730) {
        rating('1272px','1400px');
    }
	else if ($('.shitClass').width() == 540) {
        rating('1768px','1912px');
    }
    else if ($('.shitClass').width() == 328) {
        rating('1620px','1804px');
    }
    else {
        
    }
}

function rating(val1,val2) {
    let divMobile = document.getElementById('divMobile');
    $('#divMobile').addClass('elemHide');
    $('#mobile-but').removeClass('mobile-but-close');
    divMobile.style.zIndex = "1";
    $('#page1').hide();
    $('#main-div').show();
    $('#deposit').addClass('elemHide');

	$('#footer').addClass('footerTop2');
	$('#footer').removeClass('footerTop3');
	$('#footer').removeClass('footerTop4');
	$('#footer').removeClass('footerTop5');
	$('#footer').removeClass('footerTop6');
	
    $('#pageRating').show();
    $('#openMenu').addClass('elemHide');
    $('#openMenuType').addClass('elemHide');
    $('#playMenuOpen').addClass('elemHide');
    $('#playArrow').removeClass('transform-arrow');
    $('#userDeposit').removeClass('left-390');
    $('.forScrollEX').addClass('elemHide');
};

function showMenuBox() {
    if (document.getElementById('playMenuOpen').classList.contains('elemHide')) {
        $('#playArrow').addClass('transform-arrow');
        $('#playMenuOpen').removeClass('elemHide');
        $('#openMenu').addClass('elemHide');
        $('#openMenuType').addClass('elemHide');
    } else {
        $('#playArrow').removeClass('transform-arrow');
        $('#playMenuOpen').addClass('elemHide');
    }
}

function openGameInfo(){
    $('#gameDiv').show();
}

function signHchange(head1,head2){
    $('#' + head1).addClass('cursPointer');
    $('#' + head1).addClass('colorTxt-4');
    $('#' + head2).removeClass('colorTxt-4');
    $('#' + head2).removeClass('cursPointer');
    $('#signInButton').html('Sign ' + head2.substring(4));
    $('#sign' + head1.substring(4) + 'Panel').addClass('elemHide');
    $('#sign' + head2.substring(4) + 'Panel').removeClass('elemHide');
}

function countdown() {
    let elem = document.getElementById('countdown');
    //let length = 730 - (tick/10) * 12.17;
    let length = 100 - (tick + 6)/6;
    if (length < 0) length = 0;
    elem.style.width = length + "%";
    elem = document.getElementById('timer');
    length = Math.floor(60 - tick/10);
    elem.innerHTML = length + "s";
    tick = tick + 1;
    if (tick == 601)  tick = 0;
}

/*
document.addEventListener("DOMContentLoaded", function() {
    let timerId = setInterval(() => countdown(), 1000);
});
*/

function openMenu(elem) {
    $('#' + elem).removeClass('elemHide');
    $('#playArrow').removeClass('transform-arrow');
    $('#playMenuOpen').addClass('elemHide');
}

function openMenuWallet(walCount, text) {
    let elem = document.getElementById('withdrawCardMenu');
    if (elem) {
        elem.parentNode.removeChild(elem);
    }
    
    let menuParent = document.getElementById('withdrawCard');
    let menuBack = document.createElement('div');
    menuBack.id = "withdrawCardMenu";
    menuBack.className = "withdrawCardMenu";
    menuBack.style.height = walCount*48 + "px";
    menuParent.appendChild(menuBack);
    
    let i = 0;
    while (i < walCount) {
        let menuString = document.createElement('div');
        menuString.id = "menuString" + i;
        menuBack.appendChild(menuString);
        menuString.innerHTML = text[i];
        menuString.setAttribute("onClick", "closeWalletMenu('" + text[i] + "');");
        
        if (i == 0) {
            menuString.className = "firstLane M-16-500-1-5 paddingWal walMenuString";
            let menuArrow = document.createElement('div');
            menuArrow.className = "arrow-div pos5 transform-arrow";
            menuArrow.style.left = "auto";
            menuArrow.style.right = "12px";
            menuString.appendChild(menuArrow);
        }
        else if (i == walCount - 1) menuString.className = "firstLane M-16-500-1-5 paddingWal thirdLane walMenuString";
        else menuString.className = "firstLane M-16-500-1-5 paddingWal secondLane walMenuString";
        
        i++;
    }
}

function closeWalletMenu(val) {
    let elem = document.getElementById('withdrawSelectWalletTxt');
    elem.innerHTML = val;
    elem = document.getElementById('withdrawCardMenu');
    elem.parentNode.removeChild(elem);
}

function closeMenu(val2,val3,val) {
    $('#' + val2).addClass('elemHide');
    let elem = document.getElementById(val3);
    if (val != '') {
        elem.innerHTML = val;
        elem.style = "color: #ffffff;";
    }
    if (document.getElementById('betValue').innerHTML == 'Standart') $('#maxCntBet').addClass('elemHide');
    else $('#maxCntBet').removeClass('elemHide');
    //else elem.style = "color: rgba(224, 224, 255, 0.6);";
}

function changeValue() {
    let r1 = document.getElementById('myRange');
    let val = document.getElementById('winChancePerc');
    let div1 = document.getElementById('input-bet-div');
	
	if ($('.shitClass').width() == 328) {
        div1.style.width = "calc(" + r1.value*2.7 + "px)";
    }
    else {
        div1.style.width = "calc(" + r1.value*3 + "px)";   
    }
    val.innerHTML = r1.value + "%";
}

function disableRange() {
    let div1 = document.getElementById('myRange');
    div1.disabled = true;
    div1.readOnly = true;
    div1.style = "display:none;";
    $('#winChancePerc').css('color', 'rgba(224, 224, 255, 0.24');
    $('#winTxt').css('color', 'rgba(224, 224, 255, 0.24');
    $('#input-bet-div').hide();
    $('#input-bet-div-2').css('background-color', 'rgba(224, 224, 255, 0.06)');
    $('#input-bet-div-3').show();
    $('#bet_result').css('color', 'rgba(224, 224, 255, 0.24)');
    let bet_result = document.getElementById('bet_result');
    bet_result.disabled = true;
    bet_result.readOnly = true;
    $('#input-div').removeClass('input-div');
    $('#input-div').addClass('input-div-2');
    $('#input-div').css('cursor', 'default');
}

function openBetsEX() {
    if ($('.shitClass').width() == 1110) { 
        openBets('1032px','1160px','190px','190px');
    }
    else if ($('.shitClass').width() == 730) {
        openBets('1574px','1702px','364px','190px');
    }
	else if ($('.shitClass').width() == 540) {
        openBets('1580px','1724px','336px','176px');
    }
	else if ($('.shitClass').width() == 328) {
        openBets('1772px','1908px','296px','64px');
    }
    else {
        
    }
}

function openBets(val1,val2,val3,val4) {
    let divMobile = document.getElementById('divMobile');
    $('#divMobile').addClass('elemHide');
    $('#mobile-but').removeClass('mobile-but-close');
    divMobile.style.zIndex = "1";
    $('#gameCard').show();
    $('#openMenu').addClass('elemHide');
    $('#openMenuType').addClass('elemHide');
    $('#playMenuOpen').addClass('elemHide');
    $('#playArrow').removeClass('transform-arrow');
    $('#referral').addClass('elemHide');
    $('#primary').addClass('elemHide');
    $('#chgpswrd').addClass('elemHide');
    $('#slipCard').addClass('elemHide');
    $('#openSlots').show();
    $('#walletCard').addClass('elemHide');
    $('.forScrollEX').addClass('elemHide');
    $('#deposit').addClass('elemHide');
    $('#main-div').show();
    $('#page1').show();
    $('#pageRating').hide();
    $('#playMenuOpen').addClass('elemHide');
    $('#playArrow').removeClass('transform-arrow');
    $('#cardMakeBet-3').hide();
    $('#cardMakeBet-2').show();
	
	$('#card-3').addClass('card3top2');
	$('#card-3').removeClass('card3top3');
	
	$('#footer').removeClass('footerTop2');
	$('#footer').addClass('footerTop3');
	$('#footer').removeClass('footerTop4');
	$('#footer').removeClass('footerTop5');
	$('#footer').removeClass('footerTop6');
	
    $('#userWithdraw').addClass('elemHide');
    $('#logoutBut').removeClass('elemHide');
    $('#gameCard').addClass('gameCardTop-2');
    $('#gameCard').removeClass('topValveGame');
    $('#cnt-slots').hide();
    $('#cubes').hide();
    $('#chanceNum').show();
    $('#jack-div').removeClass('jack-div-2');
    $('#all-slots').hide();
    $('#percTxt').show();
    $('#slotsTxt').hide();
    $('#cnahceTxt').show();
    $('#countdown-div').hide();
    $('#valve').addClass('elemHide');
    $('#card-7').show();
    $('#userDeposit').removeClass('left-390');
    hideShit();
}

function openSlotsEX() {
    if ($('.shitClass').width() == 1110) { 
        openSlots('860px','988px','268px');
		$('#luckyNum').html('LUCKY NUMBER');
    }
    else if ($('.shitClass').width() == 730) {
        openSlots('970px','1098px','268px');
		$('#luckyNum').html('LUCKY NUMBER');
    }
	else if ($('.shitClass').width() == 540) {
        openSlots('900px','1056px','240px');
		$('#luckyNum').html('LUCKY NUM.');
    }
    else if ($('.shitClass').width() == 328) {
        openSlots('1114px','1298px','128px');
		$('#luckyNum').html('LUCKY NUMBER');
    }
    else {
        
    }
}

function openSlots(val1,val2,val3) {
    let divMobile = document.getElementById('divMobile');
    $('#divMobile').addClass('elemHide');
    $('#mobile-but').removeClass('mobile-but-close');
    divMobile.style.zIndex = "1";
    $('#openMenu').addClass('elemHide');
    $('#openMenuType').addClass('elemHide');
    $('#playMenuOpen').addClass('elemHide');
    $('#playArrow').removeClass('transform-arrow');
    $('#gameCard').show();
    $('#referral').addClass('elemHide');
    $('#primary').addClass('elemHide');
    $('#chgpswrd').addClass('elemHide');
    $('#slipCard').addClass('elemHide');
    $('#walletCard').addClass('elemHide');
    $('.forScrollEX').addClass('elemHide');
    $('#deposit').addClass('elemHide');
    $('#main-div').show();
    $('#page1').show();
    $('#pageRating').hide();
    hideShit();
    $('#cardMakeBet-2').hide();
    $('#card-7').hide();
    $('#openSlots').hide();
    $('#countdown-div').hide();
    $('#cubes').show();
    $('#cardMakeBet-3').show();
    $('#jack-div').addClass('jack-div-2');
    $('#valve').removeClass('elemHide');
    $('#playMenuOpen').addClass('elemHide');
    $('#playArrow').removeClass('transform-arrow');

	$('#card-3').removeClass('card3top2');
	$('#card-3').addClass('card3top3');
	
	$('#footer').removeClass('footerTop2');
	$('#footer').removeClass('footerTop3');
	$('#footer').addClass('footerTop4');
	$('#footer').removeClass('footerTop5');
	$('#footer').removeClass('footerTop6');
	
	$('#gameCard').addClass('topValveGame');
    $('#userWithdraw').addClass('elemHide');
    $('#logoutBut').removeClass('elemHide');
    $('#gameCard').removeClass('gameCardTop-2');
    $('#userDeposit').removeClass('left-390');
}

function hideShit() {
    $('#cardMakeBet-1').hide();
    $('#card-4').hide();
    $('#card-5').hide();
    $('#gameTipe').hide();
    $('#gameTime').hide();
}

function openDepo(page, flag) {
    let elem = document.getElementById('withdrawCardMenu');
    if (elem) {
        elem.parentNode.removeChild(elem);
    }

    $('#footer').removeClass('footerTop2');
	$('#footer').removeClass('footerTop3');
	$('#footer').removeClass('footerTop4');
	$('#footer').removeClass('footerTop5');
	$('#footer').addClass('footerTop6');
	
    $('#pname').html(page);
    $('#playArrow').removeClass('transform-arrow');
    $('#playMenuOpen').addClass('elemHide');
    $('#main-div').hide();
    $('#deposit').removeClass('elemHide');
    if (flag) {
        $('#depo_value').removeClass('depo_value_fout');
        $('#depoButton').html('Make a Deposit');
        $('#withdrawInput').addClass('depo_value_fout');
        $('#depoCard').show();
        $('#withdrawCard').addClass('elemHide');
        $('#withTxt').addClass('elemHide');
    } else {
        $('#withdrawInput').removeClass('depo_value_fout');
        $('#depoCard').hide();
        $('#withdrawCard').removeClass('elemHide');
        $('#withTxt').removeClass('elemHide');
        $('#depoButton').html('Withdraw Funds');
    }
}

function openLKEX() {
	$('#userDeposit').removeClass('left-390');
    if ($('.shitClass').width() == 1110) { 
        openLK('1270px','1398px','left-390','190px');
    }
    else if ($('.shitClass').width() == 730) {
        openLK('1472px','1522px','left-390','190px');
    }
	else if ($('.shitClass').width() == 540) {
        openLK('1864px','2008px','left-390','176px');
    }
    else if ($('.shitClass').width() == 328) {
        openLK('2154px','2338px','left-390','64px');
    }
    else {
        
    }
}

function openLK(val1,val2,val3,val4) {
    let divMobile = document.getElementById('divMobile');
    $('#divMobile').addClass('elemHide');
    $('#mobile-but').removeClass('mobile-but-close');
    divMobile.style.zIndex = "1";
    hideShit();
    $('#userDeposit').addClass(val3);
    $('#deposit').addClass('elemHide');
    $('#openMenu').addClass('elemHide');
    $('#openMenuType').addClass('elemHide');
    $('#playMenuOpen').addClass('elemHide');
    $('#playArrow').removeClass('transform-arrow');
    $('#main-div').show();
    $('#page1').show();
    $('#pageRating').hide();
    $('#cardMakeBet-2').hide();
    $('#cardMakeBet-3').hide();
    $('#valve').addClass('elemHide');
    $('#card-7').hide();
    $('#countdown-div').hide();
    $('#gameCard').hide();
    $('#logoutBut').addClass('elemHide');
    $('#userWithdraw').removeClass('elemHide');

	$('#card-3').addClass('card3top2');
	$('#card-3').removeClass('card3top3');
	
	$('#footer').removeClass('footerTop2');
	$('#footer').removeClass('footerTop3');
	$('#footer').removeClass('footerTop4');
	$('#footer').addClass('footerTop5');
	$('#footer').removeClass('footerTop6');
	
    $('#referral').removeClass('elemHide');
    $('#primary').removeClass('elemHide');
    $('#chgpswrd').removeClass('elemHide');
    $('#slipCard').removeClass('elemHide');
    $('#walletCard').removeClass('elemHide');
    $('.forScrollEX').removeClass('elemHide');
    $('#userEmail').removeClass('depo_value_fout');
    $('#userName').removeClass('depo_value_fout');
    SetRequired(false);
}

function SetRequired(param) {
    document.getElementById('userPassAppr').required = param;
    document.getElementById('userPassNew').required = param;
    document.getElementById('userPass').required = param;
}

function showcheckPrPic(div1,div2,div3) {
    let checkPrPic = document.getElementById(div1);
    if (checkPrPic.classList.contains('elemHide')) {
        $('#' + div1).removeClass('elemHide');
        $('#' + div2).addClass('checkPrSelected');
        $('#' + div2).removeClass('checkPrHover');
        document.getElementById(div3).onmouseover = function() {$('#' + div2).addClass('checkPrSelectedHover');};
        document.getElementById(div3).onmouseout = function() {$('#' + div2).removeClass('checkPrSelectedHover');};
    } else {hidecheckPrPic(div1,div2,div3);}
}

function hidecheckPrPic(div1,div2,div3) {
    $('#' + div1).addClass('elemHide');
    $('#' + div2).removeClass('checkPrSelected');
    $('#' + div2).removeClass('checkPrSelectedHover');
    document.getElementById(div3).onmouseover = function() {$('#' + div2).addClass('checkPrHover');};
    document.getElementById(div3).onmouseout = function() {$('#' + div2).removeClass('checkPrHover');};
}
    
function slipGo(flag,val) {
    $('#Slip').css('left', val);
    if (flag) {
        $('#leftSlip').addClass('disable-color');
        $('#rightSlip').removeClass('disable-color');}
    else {
        $('#leftSlip').removeClass('disable-color');
        $('#rightSlip').addClass('disable-color');}
}